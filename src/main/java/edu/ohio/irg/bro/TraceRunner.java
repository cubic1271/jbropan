package edu.ohio.irg.bro;

import edu.ohio.irg.bro.trace.TraceReader;
import edu.ohio.irg.bro.trace.analysis.BasicAnalyzer;
import org.apache.commons.cli.*;

import java.io.*;
import java.nio.channels.FileChannel;
import java.util.Scanner;

public class TraceRunner {
    public static final String DEFAULT_JSONP_CALLBACK = "$.bro.jsonp.callback";

    public static void usageAndExit(String message, Options options) {
        System.err.println(message);
        System.err.println(options);
        System.exit(-1);
    }

    public static void makeCsvFile(File base, String filename, String prefix, BasicAnalyzer contents, String postfix) throws IOException {
        File tmp = new File(base.getAbsolutePath() + File.separatorChar + filename);
        if(!tmp.exists()) {
            tmp.createNewFile();
        }
        FileWriter writer = new FileWriter(tmp);
        writer.append(prefix);
        writer.close();
        FileOutputStream rawWriter = new FileOutputStream(tmp, true);
        contents.writeCSV(rawWriter);
        rawWriter.close();
        writer = new FileWriter(tmp, true);
        writer.append(postfix);
        writer.close();
    }

    public static void makeJsonFile(File base, String filename, String prefix, BasicAnalyzer contents, String postfix) throws IOException {
        File tmp = new File(base.getAbsolutePath() + File.separatorChar + filename);
        if(!tmp.exists()) {
            tmp.createNewFile();
        }
        FileWriter writer = new FileWriter(tmp, true);
        writer.append(prefix);
        writer.close();
        FileOutputStream rawWriter = new FileOutputStream(tmp, true);
        contents.writeJSON(rawWriter);
        rawWriter.close();
        writer = new FileWriter(tmp, true);
        writer.append(postfix);
        writer.close();
    }

    public static void makeFile(File base, String filename, String contents) throws IOException {
        File tmp = new File(base.getAbsolutePath() + File.separatorChar + filename);
        if(!tmp.exists()) {
            tmp.createNewFile();
        }
        FileWriter writer = new FileWriter(tmp);
        writer.write(contents);
        writer.close();
    }

    public static String getFile(String path) throws IOException
    {
        return new Scanner(new File(path)).useDelimiter("\\Z").next();
    }

    // http://stackoverflow.com/questions/106770/standard-concise-way-to-copy-a-file-in-java
    public static void copyFile(File sourceFile, File destFile) throws IOException {
        if(!destFile.exists()) {
            destFile.createNewFile();
        }

        FileChannel source = null;
        FileChannel destination = null;

        try {
            source = new FileInputStream(sourceFile).getChannel();
            destination = new FileOutputStream(destFile).getChannel();
            destination.transferFrom(source, 0, source.size());
        }
        finally {
            if(source != null) {
                source.close();
            }
            if(destination != null) {
                destination.close();
            }
        }
    }

    public static void main(String[] args) {
        Options options = new Options();

        options.addOption(OptionBuilder.withArgName("mapfile").hasArg().withDescription("Trace map file location").create("m"));
        options.addOption(OptionBuilder.withArgName("directory").hasArg().withDescription("Directory to output results of analysis").create("o"));
        options.addOption(OptionBuilder.withArgName("tracefile").hasArg().withDescription("Trace file location").create("t"));
        options.addOption(OptionBuilder.withArgName("trace_id").hasArg().withDescription("Specifies the unique id of a coherent trial (read: set of outputs) with which to associate this data.").create("T"));
        options.addOption(new Option("h", "Print this message"));

        CommandLineParser parser = new GnuParser();
        CommandLine line = null;

        HelpFormatter formatter = new HelpFormatter();

        try
        {
            line = parser.parse(options, args);
        }
        catch(ParseException ex) {
            formatter.printHelp("jbropan.jar", options);
            System.exit(1);
        }

        if(null != line && line.hasOption('h')) {
            formatter.printHelp("jbropan.jar", options);
            System.exit(0);
        }

        TraceReader reader = new TraceReader();

        String mapFilePath = "/tmp/bro.trace.map";
        String traceFilePath = "/tmp/bro.trace.out";

        if(null != line && line.hasOption('m')) {
            mapFilePath = line.getOptionValue('m');
        }
        if(null != line && line.hasOption('t')) {
            traceFilePath = line.getOptionValue('t');
        }

        if(null != line && line.hasOption('T')) {
            System.out.println("Setting trial ID: " + line.getOptionValue('T'));
            reader.setTrialId(line.getOptionValue('T'));
        }

        if(null == mapFilePath) {
            usageAndExit("Map file path cannot be empty.", options);
        }

        if(null == traceFilePath) {
            usageAndExit("Trace file path cannot be empty.", options);
        }

        try
        {
            System.out.println("Setting map file: " + mapFilePath);
            reader.setMapFile(mapFilePath);
            System.out.println("Setting trace file: " + traceFilePath);
            reader.setTraceFile(traceFilePath);
        }
        catch(IOException ex) {
            ex.printStackTrace();
        }

        long startTime = System.currentTimeMillis();
        Thread t = new Thread(reader);
        t.start();
        try {
            t.join();
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        startTime = System.currentTimeMillis() - startTime;
        System.out.println("Processed tracefile in " + startTime + " ms");

        String outputDirectory = null;
        if(null != line && line.hasOption('o')) {
            outputDirectory = line.getOptionValue('o');
        }

        if(null != outputDirectory) {
            System.out.println("Writing output to directory: " + outputDirectory);
        }
        else {
            System.out.println("No destination for output specified.  Assuming a local outputDirectory = '/tmp/jbropan'");
            outputDirectory = "/tmp/jbropan";
        }

        File output = new File(outputDirectory);
        if(!output.exists()) {
            System.err.println("Output directory (" + outputDirectory + ") does not exist.  Attempting to create it ...");
            output.mkdirs();
        }
        if(!output.exists() || !output.canWrite()) {
            System.err.println("Unable to write to directory " + output.getAbsolutePath() + ".  Aborting...");
            System.exit(-1);
        }

        try
        {
            String callbackName = DEFAULT_JSONP_CALLBACK;
            if(line.hasOption('c')) {
                callbackName = line.getOptionValue('c');
            }

            File mapFile = new File(mapFilePath);
            File mapFileCopy = new File(output.getAbsolutePath() + File.separatorChar + "map.json");

            // We might need to inject a trialId into the bro map we get.
            String mapFilePrefix = null;
            String mapFileSuffix = null;
            if(line.hasOption('T')) {
                mapFilePrefix = "{\"trial_id\":\"" + line.getOptionValue('T') + "\", \"info\":";
            }
            else {
                mapFilePrefix = "{\"map_info\":";
            }
            mapFileSuffix = "}";

            makeFile(output, mapFileCopy.getName(), mapFilePrefix + getFile(mapFile.getAbsolutePath()) + mapFileSuffix);
            makeFile(output, "count.json", "{ \"triggerTotals\": " + reader.getCountAnalyzer().toJSON() + " }");
            makeCsvFile(output, "cpu-aggregate.csv", "", reader.getPacketAggregateAnalyzer(), "");
            makeCsvFile(output, "cpu-perpacket.csv", "", reader.getPacketPerPacketAggregateAnalyzer(), "");
            makeCsvFile(output, "memory-aggregate.csv", "", reader.getMemoryAggregateAnalyzer(), "");
            makeCsvFile(output, "memory-perpacket.csv", "", reader.getMemoryPerPacketAggregateAnalyzer(), "");

//            makeFile(output, "call_chain.json", "{ \"call_chain\": " + reader.getCallChainAnalyzer().toJSON() + " }");
//            makeFile(output, "script_timing.json", "{ \"script_timing\": " + reader.getScriptTimingAnalyzer().toJSON() + " }");
//            makeFile(output, "event_path.json", "{ \"event_path\": " + reader.getEventPathAnalyzer().toJSON() + " }");
//            makeFile(output, "packet_chain.json", "{ \"packet_chain\": ", reader.getPacketChainAnalyzer(), " }");
        }
        catch(IOException ex) {
            throw new RuntimeException(ex);
        }
    }
}
