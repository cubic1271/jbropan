package edu.ohio.irg.bro.trace.analysis;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import edu.ohio.irg.bro.trace.TraceEntry;
import edu.ohio.irg.bro.trace.json.PacketAggregateAnalyzerAdapter;
import edu.ohio.irg.bro.trace.map.FunctionMapping;
import edu.ohio.irg.bro.trace.util.EventNameResolver;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Stack;

public class PacketAggregateAnalyzer extends BasicAnalyzer {
    protected ArrayList<Long> triggerTotals = new ArrayList<Long>();
    protected ArrayList<Long> packetTotals = new ArrayList<Long>();
    protected ArrayList<Long> timerTotals = new ArrayList<Long>();
    protected ArrayList<Long> scriptTotals = new ArrayList<Long>();
    protected ArrayList<HashMap<String, Long>> functionTotals = new ArrayList<HashMap<String, Long>>();

    protected HashMap<String, Stack<Long>> functionStart = new HashMap<String, Stack<Long>>();
    protected HashMap<String, Long> functionSum = new HashMap<String, Long>();
    protected HashSet<String> uniqueFileList = new HashSet<String>();
    protected Stack<Long> scriptStart = new Stack<Long>();
    protected Long scriptSum = 0L;
    protected Long packetStart = 0L;
    protected Long packetSum = 0L;
    protected Long triggerStart = 0L;
    protected Long triggerSum = 0L;
    protected Long timerStart = 0L;
    protected Long timerSum = 0L;

    protected boolean perPacketCounters = true;

    public static final int DEFAULT_WEIGHT = 1000;
    public static final String DEFAULT_NAME = "PacketAggregateAnalyzer";

    public PacketAggregateAnalyzer() {
        this.weight = DEFAULT_WEIGHT;
        this.name = DEFAULT_NAME;
    }

    public PacketAggregateAnalyzer(int weight) {
        this.weight = weight;
        this.name = DEFAULT_NAME;
    }

    public PacketAggregateAnalyzer(String name) {
        this.weight = DEFAULT_WEIGHT;
        this.name = name;
    }

    public PacketAggregateAnalyzer(String name, boolean perPacket) {
        this.weight = DEFAULT_WEIGHT;
        this.perPacketCounters = perPacket;
        this.name = name;
    }

    public PacketAggregateAnalyzer(int weight, String name) {
        this.weight = weight;
        this.name = name;
    }

    public ArrayList<Long> getTriggerTotals() {
        return new ArrayList<Long>(this.triggerTotals);
    }

    public ArrayList<Long> getPacketTotals() {
        return new ArrayList<Long>(this.packetTotals);
    }

    public HashSet<String> getUniqueFileList() {
        return new HashSet<String>(uniqueFileList);
    }

    @Override
    public boolean update(TraceEntry entry) {
        super.update(entry);
        if(entry.isComponent()) {
            if(entry.getGlobalId().getId().getFunctionId() == TraceEntry.ID_BEGIN_PACKET) {
                // start of the current packet.  record triggerTotals for this packet.
                functionStart = new HashMap<String, Stack<Long>>();
                packetStart = entry.getPapiVirtual();
                triggerStart = 0L;
                timerStart = 0L;
                if(perPacketCounters) {
                    functionSum = new HashMap<String, Long>();
                    triggerSum = 0L;
                    timerSum = 0L;
                    scriptSum = 0L;
                }
            }
            else if(entry.getGlobalId().getId().getFunctionId() == TraceEntry.ID_BEGIN_TIMER) {
                timerStart = entry.getPapiVirtual();
            }
            else if(entry.getGlobalId().getId().getFunctionId() == TraceEntry.ID_END_TIMER) {
                assert(timerStart > 0);
                timerSum += (entry.getPapiVirtual() - timerStart);
            }
            else if(entry.getGlobalId().getId().getFunctionId() == TraceEntry.ID_BEGIN_TRIGGER) {
                triggerStart = entry.getPapiVirtual();
            }
            else if(entry.getGlobalId().getId().getFunctionId() == TraceEntry.ID_END_TRIGGER) {
                assert(triggerStart > 0);
                triggerSum += (entry.getPapiVirtual() - triggerStart);
            }
            else if(entry.getGlobalId().getId().getFunctionId() == TraceEntry.ID_END_PACKET) {
                // end of the current packet.  record triggerTotals for this packet.
                if(packetStart > 0) {
                    if(triggerStart > 0) {
                        triggerTotals.add(triggerSum);
                    }
                    else {
                        triggerTotals.add(0L);
                    }
                    if(timerStart > 0) {
                        timerTotals.add(timerSum);
                    }
                    else {
                        timerTotals.add(0L);
                    }
                    scriptTotals.add(scriptSum);
                    // add a copy of our hashmap
                    functionTotals.add(new HashMap<String, Long>(functionSum));
                    packetSum += entry.getPapiVirtual() - packetStart;
                    if(perPacketCounters) {
                        packetTotals.add(entry.getPapiVirtual() - packetStart);
                    }
                    else {
                        packetTotals.add(packetSum);
                    }
                }
            }
        }
        else {
            // script call outside of packet call is discarded.
            if(packetStart.equals(0L)) {
                return true;
            }
            if(entry.isCall()) {
                scriptStart.push(entry.getPapiVirtual());
                FunctionMapping.BodyMapping mapping = resolver.getBodyInfo(entry.getId());
                if(null == mapping) {
                    // If we have a function entry with no explicit bodies, generate one and give it an ID of 0.
                    FunctionMapping tmpMap = resolver.getFunctionInfo(entry.getId());
                    if(null != tmpMap) {
                        mapping = tmpMap.new BodyMapping();
                        mapping.setFilename(tmpMap.getFilename());
                        mapping.setId(0);
                    }
                }
                if(null != mapping) {
                    String source = mapping.getFilename();
                    if (null != source) {
                        source = FunctionMapping.getRelativeScriptName(source);
                        if( !functionStart.containsKey(source) ) {
                            Stack<Long> stack = new Stack<Long>();
                            stack.push(entry.getPapiVirtual());
                            functionStart.put(source, stack);
                        }
                        else {
                            functionStart.get(source).push(entry.getPapiVirtual());
                        }
                    }
                    else {
                        System.err.println("[WARN] unable to resolve filename for function: " + entry.getId());
                    }
                    // System.out.println("[INFO] Resolved: " + entry.getId());
                }
                else {
                    System.err.println("[WARN] unable to resolve function: " + entry.getId());
                }
            }
            else if(entry.isReturn()) {
                assert(resolver != null);
                // we're returning from the last frame on the stack, so compute time *only* from this frame.
                if(scriptStart.size() == 1) {
                    scriptSum += entry.getPapiVirtual() - scriptStart.peek();
                }
                if(scriptStart.size() > 0) {
                    scriptStart.pop();
                }
                FunctionMapping.BodyMapping mapping = resolver.getBodyInfo(entry.getId());
                if(null != mapping) {
                    String source = mapping.getFilename();
                    if(null != source) {
                        source = FunctionMapping.getRelativeScriptName(source);
                        if(!uniqueFileList.contains(source)) {
                            uniqueFileList.add(source);
                        }
                        if(!functionStart.containsKey(source)) {
                            // saw a return for a function we hadn't called, so no real good frame of reference here.  Thus, discard.
                            return true;
                        }
                        if(functionSum.containsKey(source)) {
                            // incremental addition since we've seen this function multiple times for this packet
                            functionSum.put(source, (entry.getPapiVirtual() - functionStart.get(source).peek()) + functionSum.get(source));
                            functionStart.get(source).pop();
                        }
                        else {
                            // first time we've seen this function for this packet
                            functionSum.put(source, entry.getPapiVirtual() - functionStart.get(source).peek());
                            functionStart.get(source).pop();
                        }
                    }

                }
            }
        }
        return true;
    }

    @Override
    public void clear() {
        super.clear();
        triggerTotals.clear();
        packetTotals.clear();
        scriptTotals.clear();
        timerTotals.clear();
        functionTotals.clear();
        scriptStart.clear();
        triggerStart = 0L;
        packetStart = 0L;
        functionStart.clear();
        triggerSum = 0L;
        packetSum = 0L;
        scriptSum = 0L;
        functionSum.clear();
        timerSum = 0L;
        uniqueFileList.clear();
    }

    public String toString() {
        return "PacketAggregateAnalyzer with " + triggerTotals.size() + " unique triggerTotals calculated across " + counter + " entries.";
    }

    public HashMap<String, Integer> difference(CountAnalyzer target, EventNameResolver resolver) {
        return null;
    }

    @Override
    public String toJSON() {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(PacketAggregateAnalyzer.class, new PacketAggregateAnalyzerAdapter(getParent().getResolver()))
                .create();
        return gson.toJson(this);
    }

    @Override
    public String toCSV() {
         return "";
    }

    @Override
    public void writeCSV(OutputStream output) throws IOException {
        OutputStreamWriter writer = new OutputStreamWriter(output);

        System.err.println("Writing CSV ... (" + packetTotals.size() + " lines)");
        writer.write("# Aggregate output format.  Output is raw virtual count.\n");
        writer.write("# pktid,total,triggers,timers,scripts");
        for(String curr : uniqueFileList) {
            writer.write("," + curr);
        }
        writer.write("\n");

        for(int i = 0; i < packetTotals.size(); ++i) {
            writer.write("" + i);
            writer.write("," + packetTotals.get(i));
            writer.write("," + triggerTotals.get(i));
            writer.write("," + timerTotals.get(i));
            writer.write("," + scriptTotals.get(i));
            for(String curr : uniqueFileList) {
                if(functionTotals.get(i).containsKey(curr)) {
                    writer.write("," + functionTotals.get(i).get(curr));
                }
                else {
                    writer.write(",-");
                }
            }
            writer.write("\n");
        }
        writer.flush();
        writer.close();
    }
}
