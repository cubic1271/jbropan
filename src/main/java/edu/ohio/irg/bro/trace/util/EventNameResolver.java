package edu.ohio.irg.bro.trace.util;

import edu.ohio.irg.bro.trace.TraceEntry;
import edu.ohio.irg.bro.trace.map.FunctionMapping;
import edu.ohio.irg.bro.trace.map.Mapping;
import edu.ohio.irg.bro.trace.state.GlobalEventIdentifier;
import edu.ohio.irg.bro.trace.state.LocalEventIdentifier;

import java.util.HashMap;

public class EventNameResolver
{
    protected HashMap<GlobalEventIdentifier, Mapping> mappings = new HashMap<GlobalEventIdentifier, Mapping>();

    public EventNameResolver() {

    }

    public EventNameResolver(EventNameResolver resolver) {
        this.mappings = new HashMap<GlobalEventIdentifier, Mapping>(resolver.mappings);
    }

    public Integer size() {
        return mappings.size();
    }

    public boolean has(GlobalEventIdentifier entry) {
        return mappings.containsKey(entry);
    }

    public void setMapping(HashMap<GlobalEventIdentifier, Mapping> mapping) {
        mappings = new HashMap<GlobalEventIdentifier, Mapping>(mapping);
    }

    public boolean isBuiltin(LocalEventIdentifier id, GlobalEventIdentifier.EntryType type) {
        if(type != GlobalEventIdentifier.EntryType.SCRIPT) {
            return false;
        }
        return true;
    }

    public String getName(TraceEntry entry) {
        return getName(entry.getId(), entry.getType());
    }

    public String getName(GlobalEventIdentifier entry) {
        return getName(entry.getId(), entry.getType());
    }

    public FunctionMapping getFunctionInfo(LocalEventIdentifier id) {
        if(mappings.containsKey(new GlobalEventIdentifier(id, GlobalEventIdentifier.EntryType.SCRIPT))) {
            return (FunctionMapping)mappings.get(new GlobalEventIdentifier(id, GlobalEventIdentifier.EntryType.SCRIPT));
        }
        return null;
    }

    public String getName(LocalEventIdentifier id, GlobalEventIdentifier.EntryType type) {
        if(mappings.containsKey(new GlobalEventIdentifier(id, type))) {
            return mappings.get(new GlobalEventIdentifier(id, type)).getName();
        }
        return null;
    }

    public FunctionMapping.BodyMapping getBodyInfo(LocalEventIdentifier id) {
        // we only look for the base entry in the mapping, *not* including the subtype
        if(mappings.containsKey(new GlobalEventIdentifier(new LocalEventIdentifier(id.getFunctionId(), 0), GlobalEventIdentifier.EntryType.SCRIPT))) {
            return getFunctionInfo(new LocalEventIdentifier(id.getFunctionId(), 0)).getBodyById(id.getBodyId());
        }
        else {
            System.err.println("[WARN] Unknown id: " + id);
        }
        return null;
    }
}
