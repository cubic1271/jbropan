package edu.ohio.irg.bro.trace.analysis.timing;

import edu.ohio.irg.bro.trace.TraceEntry;

import java.util.ArrayList;

public class ChainTimingAccumulator {
    public static final int TIME_PAPI_VIRTUAL = 0;
    public static final int TIME_RUSAGE_SYSTEM = 1;
    public static final int TIME_RUSAGE_USER = 2;

    public static final String[] TIME_FIELD_NAMES = {"papiVirtual", "rusageSystem", "rusageUser"};

    public static final int TIME_NUM_COUNTERS = 3;

    protected int count;
    protected ArrayList<double[]> sums = new ArrayList<double[]>();
    protected ArrayList<double[]> averages = new ArrayList<double[]>();
    protected ArrayList<double[]> variances = new ArrayList<double[]>();

    protected double[] chainSums;
    protected double[] chainAverages;
    protected double[] chainVariances;

    public ChainTimingAccumulator() {

    }

    public void init(ArrayList<TraceEntry> entries) {
        if(sums.size() == 0) {
            chainSums = new double[TIME_NUM_COUNTERS];
            chainAverages = new double[TIME_NUM_COUNTERS];
            chainVariances = new double[TIME_NUM_COUNTERS];
            for(int i = 0; i < TIME_NUM_COUNTERS; ++i) {
                chainSums[i] = 0;
                chainAverages[i] = 0;
                chainVariances[i] = 0;
            }

            count = 0;
            for(int i = 0; i < entries.size() - 1; ++i) {
                double[] values = new double[TIME_NUM_COUNTERS];
                for(int curr = 0; curr < TIME_NUM_COUNTERS; ++curr) {
                    values[curr] = 0;
                }
                sums.add(values);
            }

            for(int i = 0; i < entries.size() - 1; ++i) {
                double[] values = new double[TIME_NUM_COUNTERS];
                for(int curr = 0; curr < TIME_NUM_COUNTERS; ++curr) {
                    values[curr] = 0;
                }
                averages.add(values);
            }

            for(int i = 0; i < entries.size() - 1; ++i) {
                double[] values = new double[TIME_NUM_COUNTERS];
                for(int curr = 0; curr < TIME_NUM_COUNTERS; ++curr) {
                    values[curr] = 0;
                }
                variances.add(values);
            }
        }
    }

    public void accumulate(ArrayList<TraceEntry> entries) {
        init(entries);
        ++count;
        // per-transition statistics
        for(int i = 1; i < entries.size(); ++i) {
            double[] curr = getTimings(entries.get(i));
            double[] base = getTimings(entries.get(i - 1));

            for(int counter = 0; counter < TIME_NUM_COUNTERS; ++counter) {
                sums.get(i - 1)[counter] += curr[counter] - base[counter];
                double delta = (curr[counter] - base[counter]) - averages.get(i - 1)[counter];
                averages.get(i - 1)[counter] = averages.get(i - 1)[counter] + (delta / (double)count);
                variances.get(i - 1)[counter] = variances.get(i - 1)[counter] + delta * ((curr[counter] - base[counter]) - averages.get(i - 1)[counter]);
            }
        }

        // overall statistics
        for(int counter = 0; counter < TIME_NUM_COUNTERS; ++counter) {
            double[] curr = getTimings(entries.get(entries.size() - 1));
            double[] base = getTimings(entries.get(0));
            chainSums[counter] += curr[counter] - base[counter];
            double delta = (curr[counter] - base[counter]) - chainAverages[counter];
            chainAverages[counter] = chainAverages[counter] + (delta / (double)count);
            chainVariances[counter] = chainVariances[counter] + delta * ((curr[counter] - base[counter]) - chainAverages[counter]);
        }
    }

    public ArrayList<double[]> getSums() {
        return sums;
    }

    public ArrayList<double[]> getAverages() {
        return averages;
    }

    public ArrayList<double[]> getVariances() {
        return variances;
    }

    public double[] getChainSums() {
        return chainSums;
    }

    public double[] getChainAverages() {
        return chainAverages;
    }

    public double[] getChainVariances() {
        return chainVariances;
    }

    public void clear() {
        chainSums = new double[TIME_NUM_COUNTERS];
        chainVariances = new double[TIME_NUM_COUNTERS];
        chainAverages = new double[TIME_NUM_COUNTERS];
        sums = new ArrayList<double[]>();
        averages = new ArrayList<double[]>();
        variances = new ArrayList<double[]>();
        count = 0;
    }

    public double[] getTimings(TraceEntry entry) {
        double[] rval = new double[TIME_NUM_COUNTERS];
        rval[TIME_PAPI_VIRTUAL] = entry.getPapiVirtual();
        rval[TIME_RUSAGE_USER] = entry.getUserTime();
        rval[TIME_RUSAGE_SYSTEM] = entry.getSystemTime();
        return rval;
    }
}
