package edu.ohio.irg.bro.trace.json;

import com.google.gson.*;
import edu.ohio.irg.bro.trace.analysis.CallChainAnalyzer;
import edu.ohio.irg.bro.trace.analysis.CountAnalyzer;
import edu.ohio.irg.bro.trace.state.ChainContainer;
import edu.ohio.irg.bro.trace.state.GlobalEventIdentifier;
import edu.ohio.irg.bro.trace.util.EventNameResolver;

import java.lang.reflect.Type;
import java.util.HashMap;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class CallChainAnalyzerAdapter implements JsonSerializer<CallChainAnalyzer> {
    protected transient EventNameResolver resolver;

    public CallChainAnalyzerAdapter(EventNameResolver resolver) {
        this.resolver = resolver;
    }

    @Override
    public JsonElement serialize(CallChainAnalyzer src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject obj = new JsonObject();
        obj.addProperty("trial_id", src.getParent().getTrialId());
        obj.addProperty("count", src.getCounter());

        JsonObject chains = new JsonObject();

        HashMap<ChainContainer, Integer> counts = src.getChainCounts();

        for(ChainContainer container : counts.keySet()) {
            JsonObject curr = new JsonObject();
            int count = counts.get(container);
            curr.addProperty("count", count);

            JsonArray callArray = new JsonArray();
            for(GlobalEventIdentifier id : container) {
                callArray.add(new JsonPrimitive(id.toString()));
            }

            curr.add("calls", callArray);

            chains.add(container.toString(), curr);
        }
        obj.add("chains", chains);

        return obj;
    }
}
