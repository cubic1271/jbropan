package edu.ohio.irg.bro.trace.state;

import edu.ohio.irg.bro.trace.TraceEntry;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class MemoryContainer {

    private Long mallocSize;
    private Long mallocCalls;
    private Long freeCalls;

    public MemoryContainer() {
        this(0L, 0L, 0L);
    }

    public MemoryContainer(TraceEntry entry) {
        this(entry.getMallocSize(), entry.getMallocCalls(), entry.getFreeCalls());
    }

    public MemoryContainer(Long mallocSize, Long mallocCalls, Long freeCalls) {
        this.mallocSize = mallocSize;
        this.mallocCalls = mallocCalls;
        this.freeCalls = freeCalls;
    }

    public MemoryContainer sum(Long mallocSize, Long mallocCalls, Long freeCalls) {
        return sum(new MemoryContainer(mallocSize, mallocCalls, freeCalls));
    }

    public MemoryContainer sum(MemoryContainer target) {
        return new MemoryContainer(this.mallocSize + target.mallocSize, this.mallocCalls + target.mallocCalls, this.freeCalls + target.freeCalls);
    }

    public MemoryContainer diff(MemoryContainer target) {
        return new MemoryContainer(this.mallocSize - target.mallocSize, this.mallocCalls - target.mallocCalls, this.freeCalls - target.freeCalls);
    }

    public Long getMallocSize() {
        return mallocSize;
    }

    public Long getMallocCalls() {
        return mallocCalls;
    }

    public Long getFreeCalls() {
        return freeCalls;
    }

    @Override
    public String toString() {
        return "" + this.mallocCalls + " / " + this.freeCalls + " : " + this.mallocSize;
    }

    @Override
    public boolean equals(Object eq) {
        if(eq == null) {
            return false;
        }
        if(eq == this) {
            return true;
        }
        if (!(eq instanceof MemoryContainer)) {
            return false;
        }

        MemoryContainer container = (MemoryContainer)eq;

        return     container.freeCalls.equals(this.freeCalls)
                && container.mallocCalls.equals(this.mallocCalls)
                && container.mallocSize.equals(this.mallocSize);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(41, 849)
                .append(this.freeCalls)
                .append(this.mallocCalls)
                .append(this.mallocSize)
                .toHashCode();
    }
}
