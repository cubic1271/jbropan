package edu.ohio.irg.bro.trace;

import edu.ohio.irg.bro.trace.state.GlobalEventIdentifier;
import edu.ohio.irg.bro.trace.state.LocalEventIdentifier;
import edu.ohio.irg.bro.trace.util.EventNameResolver;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class TraceEntry {
    public static final int OPTION_FTYPE = 1;
    public static final int OPTION_NEW_TIMESTAMP = 2;
    public static final int OPTION_RESOURCE_USAGE = 4;
    public static final int OPTION_FCALL = 64;
    public static final int OPTION_FRETURN = 128;
    public static final int OPTION_PAPI_TIME = 256;
    public static final int OPTION_COMPONENT_EVENT = 512;
    public static final int OPTION_MEMORY_STATS = 1024;
    public static final int ID_BEGIN_PACKET = 64000;
    public static final int ID_END_PACKET = 64001;
    public static final int ID_BEGIN_TRIGGER = 64006;
    public static final int ID_END_TRIGGER = 64007;
    public static final int ID_BEGIN_TIMER = 64008;
    public static final int ID_END_TIMER = 64009;

    private LocalEventIdentifier id;
    private int options;
    private long papiVirtual;
    private long ts;
    private long userTime;
    private long systemTime;
    private long maxRss;
    private long mallocCalls;
    private long mallocSize;
    private long freeCalls;

    public void adjustPapiTimestamps(double virtualBase) {
        this.papiVirtual -= virtualBase;
    }

    /**
     * Loads a single trace entry.
     *
     * @param buffer the current trace file buffer
     */
    public void populate(ByteBuffer buffer) {
        if(buffer.order() == ByteOrder.LITTLE_ENDIAN) {
            this.options = buffer.getInt();
            short fid = buffer.getShort();
            short bid = buffer.getShort();
            this.id = new LocalEventIdentifier(fid, bid);
        }
        else {
            short bid = buffer.getShort();
            short fid = buffer.getShort();
            this.id = new LocalEventIdentifier(fid, bid);
            this.options = buffer.getInt();
        }

        if((this.options & OPTION_MEMORY_STATS) != 0) {
            this.mallocCalls = buffer.getLong();
            this.mallocSize = buffer.getLong();
            this.freeCalls = buffer.getLong();
        }

        if((this.options & OPTION_PAPI_TIME) != 0) {
            this.papiVirtual = buffer.getLong();
        }

        if((this.options & OPTION_NEW_TIMESTAMP) != 0) {
            this.ts = buffer.getLong();
        }

        if((this.options & OPTION_RESOURCE_USAGE) != 0) {
            this.userTime = buffer.getLong();
            this.systemTime = buffer.getLong();
            this.maxRss = buffer.getLong();
        }

        // every event must either be a call, a return, or a component event.
        assert( (this.options & OPTION_FCALL) != 0
                || (this.options & OPTION_FRETURN) != 0
                || (this.options & OPTION_COMPONENT_EVENT) != 0);
    }

    public int size() {
        return 8 + ((options & OPTION_RESOURCE_USAGE) != 0 ? 24 : 0)
                 + ((options & OPTION_PAPI_TIME) != 0 ? 16 : 0)
                 + ((options & OPTION_NEW_TIMESTAMP) != 0 ? 8 : 0);
    }


    public boolean isComponent() {
        return ((options & OPTION_COMPONENT_EVENT) != 0);
    }

    public boolean isFunction() {
        return ((options & OPTION_FCALL) != 0)
                || ((options & OPTION_FRETURN) != 0);
    }

    public boolean isCall() {
        return ((options & OPTION_FCALL) != 0);
    }

    public boolean isReturn() {
        return ((options & OPTION_FRETURN) != 0);
    }

    public boolean isNetworkTimestampUpdated() {
        return ((options & OPTION_NEW_TIMESTAMP) != 0);
    }

    public GlobalEventIdentifier getGlobalId() {
        return new GlobalEventIdentifier(getId(), getType());
    }

    public GlobalEventIdentifier.EntryType getType() {
        return (isFunction() ? GlobalEventIdentifier.EntryType.SCRIPT : GlobalEventIdentifier.EntryType.COMPONENT);
    }

    public String resolveName(EventNameResolver resolver) {
        if(id == null) {
            return "(none)";
        }
        return resolver.getName(getId(), getType());
    }

    public String resolveName() {
        return (getType() == GlobalEventIdentifier.EntryType.SCRIPT) ? "F:" + getId() : "C:" + getId();
    }

    public String toString(EventNameResolver resolver) {
        StringBuilder result = new StringBuilder();
        result.append("[").append((resolver != null && resolver.has(getGlobalId()) ? resolver.getName(getGlobalId()) : resolveName())).append("]");
        if((options & OPTION_MEMORY_STATS) != 0) {
            result.append(" +M:").append(this.mallocCalls).append("/").append(this.freeCalls).append("  (").append(this.mallocSize).append(")");
        }
        if((options & OPTION_PAPI_TIME) != 0) {
            result.append(" +P:").append(this.papiVirtual);
        }
        if((options & OPTION_RESOURCE_USAGE) != 0) {
            result.append(" +R:").append(this.userTime).append("/").append(this.systemTime).append("-").append(this.maxRss);
        }
        if((options & OPTION_NEW_TIMESTAMP) != 0) {
            result.append(" +T:").append(this.ts);
        }
        if((options & OPTION_COMPONENT_EVENT) != 0) {
            result.append(" +C");
        }
        if((options & OPTION_FCALL) != 0) {
            result.append(" +call");
        }
        if((options & OPTION_FRETURN) != 0) {
            result.append(" +return");
        }
        return result.toString();
    }

    public String toString() {
        return toString(null);
    }

    public long getPapiVirtual() {
        return papiVirtual;
    }

    public double getPapiVirtualSec() {
        return papiVirtual / 1.0e6;
    }

    public LocalEventIdentifier getId() {
        return id;
    }

    public int getOptions() {
        return options;
    }

    public long getTs() {
        return ts;
    }

    public long getMallocCalls() {
        return mallocCalls;
    }

    public long getMallocSize() {
        return mallocSize;
    }

    public long getFreeCalls() {
        return freeCalls;
    }

    public long getUserTime() {
        return userTime;
    }

    public long getSystemTime() {
        return systemTime;
    }

    public long getMaxRss() {
        return maxRss;
    }
}
