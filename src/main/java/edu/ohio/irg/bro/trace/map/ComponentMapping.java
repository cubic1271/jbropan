package edu.ohio.irg.bro.trace.map;

import java.util.ArrayList;

public class ComponentMapping implements Mapping {
    ArrayList<ComponentEntryMapping> entries;
    int id;
    String name;

    public ComponentMapping() {
        entries = new ArrayList<ComponentEntryMapping>();
    }

    public ArrayList<ComponentEntryMapping> getEntries() {
        return new ArrayList<ComponentEntryMapping>(entries);
    }

    public void setEntries(ArrayList<ComponentEntryMapping> list) {
        entries = new ArrayList<ComponentEntryMapping>(list);
    }

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(int value) {
        this.id = value;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String text) {
        this.name = text;
    }

    class ComponentEntryMapping {
        String name;
        int id;

        public String getName() {
            return name;
        }

        public void setName(String text) {
            name = text;
        }

        public Integer getId() {
            return id;
        }

        public void setId(int value) {
            id = value;
        }
    }
}
