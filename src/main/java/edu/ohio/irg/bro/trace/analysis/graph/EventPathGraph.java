package edu.ohio.irg.bro.trace.analysis.graph;

import edu.ohio.irg.bro.trace.state.GlobalEventIdentifier;

import java.util.ArrayList;
import java.util.HashMap;

public class EventPathGraph {
    // FIXME: use something more elegant (or find a graph library that *ISN'T* LGPL'd)...
    HashMap<GlobalEventIdentifier, HashMap<GlobalEventIdentifier, EventPathEdge>> edges =
            new HashMap<GlobalEventIdentifier, HashMap<GlobalEventIdentifier, EventPathEdge>>();

    public EventPathGraph() {

    }

    public EventPathGraph(EventPathGraph g2) {
        for(GlobalEventIdentifier source : g2.edges.keySet()) {
            for(GlobalEventIdentifier sink : g2.edges.get(source).keySet()) {
                add(source, sink);
                edges.get(source).get(sink).add(g2.edges.get(source).get(sink).getWeight());
            }
        }
    }

    public ArrayList<GlobalEventIdentifier> getVertices() {
        ArrayList<GlobalEventIdentifier> list = new ArrayList<GlobalEventIdentifier>();
        for(GlobalEventIdentifier curr : edges.keySet()) {
            list.add(new GlobalEventIdentifier(curr));
        }
        return list;
    }

    public ArrayList<EventPathEdge> getEdges(GlobalEventIdentifier vertex) {
        ArrayList<EventPathEdge> list = new ArrayList<EventPathEdge>();
        for(GlobalEventIdentifier curr : edges.get(vertex).keySet()) {
            list.add(edges.get(vertex).get(curr));
        }
        return list;
    }

    public boolean contains(GlobalEventIdentifier source, GlobalEventIdentifier sink) {
        return edges.containsKey(source) && edges.get(source).containsKey(sink);
    }

    public void add(GlobalEventIdentifier source, GlobalEventIdentifier sink) {
        if(contains(source, sink)) {
            return;
        }
        if(!edges.containsKey(source)) {
            edges.put(source, new HashMap<GlobalEventIdentifier, EventPathEdge>());
        }
        if(!edges.get(source).containsKey(sink)) {
            edges.get(source).put(sink, new EventPathEdge(source, sink));
        }
    }

    public void increment(GlobalEventIdentifier source, GlobalEventIdentifier sink) {
        if(contains(source, sink)) {
            edges.get(source).get(sink).increment();
        }
    }

    public void clear() {
        edges.clear();
    }

    public Integer getCount(GlobalEventIdentifier source, GlobalEventIdentifier sink) {
        if(contains(source, sink)) {
            return new Long(Math.round(edges.get(source).get(sink).getWeight())).intValue();
        }

        return -1;
    }
}
