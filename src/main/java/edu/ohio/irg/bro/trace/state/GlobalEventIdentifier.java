package edu.ohio.irg.bro.trace.state;


import edu.ohio.irg.bro.trace.TraceEntry;
import edu.ohio.irg.bro.trace.util.EventNameResolver;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class GlobalEventIdentifier {
    protected LocalEventIdentifier id;
    protected EntryType type;

    public GlobalEventIdentifier(GlobalEventIdentifier copy)
    {
        this.id = new LocalEventIdentifier(copy.id);
        this.type = copy.type;
    }

    public GlobalEventIdentifier(LocalEventIdentifier id, EntryType type)
    {
        this.id = new LocalEventIdentifier(id);
        this.type = type;
    }

    public GlobalEventIdentifier(TraceEntry entry)
    {
        this.id = new LocalEventIdentifier(entry.getId());
        this.type = entry.isFunction() ? EntryType.SCRIPT : EntryType.COMPONENT;
    }

    public LocalEventIdentifier getId() {
        return this.id;
    }

    public EntryType getType() {
        return this.type;
    }

    @Override
    public String toString() {
        return this.type == EntryType.COMPONENT ? "C: " + getId() : "F: " + getId();
    }

    public String toString(EventNameResolver resolver) {
        return resolver.getName(this) != null ? resolver.getName(this) : toString();
    }

    @Override
    public boolean equals(Object eq) {
        if(eq == null) {
            return false;
        }
        if(eq == this) {
            return true;
        }
        if (!(eq instanceof GlobalEventIdentifier)) {
            return false;
        }

        GlobalEventIdentifier entry = (GlobalEventIdentifier) eq;

        return  entry.type.equals(this.type)
                && entry.id.equals(this.id);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(41, 319)
                .append(this.type)
                .append(this.id)
                .toHashCode();
    }

    public static enum EntryType {
        SCRIPT,
        COMPONENT,
        ALL
    }
}