package edu.ohio.irg.bro.trace.analysis;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import edu.ohio.irg.bro.trace.TraceEntry;
import edu.ohio.irg.bro.trace.analysis.timing.EventTiming;
import edu.ohio.irg.bro.trace.analysis.timing.EventTimingContainer;
import edu.ohio.irg.bro.trace.json.ScriptTimingAnalyzerAdapter;
import edu.ohio.irg.bro.trace.state.GlobalEventIdentifier;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Stack;

public class ScriptTimingAnalyzer extends BasicAnalyzer {
    public static final int DEFAULT_WEIGHT = 1000;
    public static final String DEFAULT_NAME = "ScriptTimingAnalyzer";

    Stack<GlobalEventIdentifier> scriptStack = new Stack<GlobalEventIdentifier>();
    Stack<EventTiming> timingStack = new Stack<EventTiming>();
    EventTimingContainer timings = new EventTimingContainer();

    private int accumulateCounter = 0;

    public ScriptTimingAnalyzer() {
        this.weight = DEFAULT_WEIGHT;
        this.name = DEFAULT_NAME;
    }

    public ScriptTimingAnalyzer(int weight) {
        this.weight = weight;
        this.name = DEFAULT_NAME;
    }

    public ScriptTimingAnalyzer(String name) {
        this.weight = DEFAULT_WEIGHT;
        this.name = name;
    }

    public ScriptTimingAnalyzer(int weight, String name) {
        this.weight = weight;
        this.name = name;
    }

    public EventTimingContainer getTimings() {
        return timings;
    }

    @Override
    public boolean update(TraceEntry entry) {
        super.update(entry);
        GlobalEventIdentifier currId = entry.getGlobalId();
        if(entry.isCall()) {
            scriptStack.push(currId);
            timingStack.push(new EventTiming(entry));
        }
        else if(entry.isReturn()) {
            if(scriptStack.empty()) {
                return true;
            }

            if(! scriptStack.peek().equals(currId)) {
                return true;
            }

            scriptStack.pop();
            EventTiming base = timingStack.pop();
            EventTiming curr = new EventTiming(entry);

            ++accumulateCounter;
            timings.accumulate(currId, curr, base);
        }
        return true;
    }

    @Override
    public void clear() {
        super.clear();
        scriptStack.clear();
        timingStack.clear();
        timings.clear();
        accumulateCounter = 0;
    }

    @Override
    public String toString() {
        return "ScriptTimingAnalyzer with " + timings.size() + " buckets consisting of " + accumulateCounter + " data points after processing " + counter + " entries.";
    }

    @Override
    public String toJSON() {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(ScriptTimingAnalyzer.class, new ScriptTimingAnalyzerAdapter(getParent().getResolver()))
                .create();
        return gson.toJson(this);
    }
}
