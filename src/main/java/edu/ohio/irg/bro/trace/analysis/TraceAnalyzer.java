package edu.ohio.irg.bro.trace.analysis;

import edu.ohio.irg.bro.trace.TraceEntry;
import edu.ohio.irg.bro.trace.TraceReader;
import edu.ohio.irg.bro.trace.state.GlobalEventIdentifier;
import edu.ohio.irg.bro.trace.util.EventNameResolver;

import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Comparator;

public interface TraceAnalyzer
{
    public static class WeightComparator implements Comparator<TraceAnalyzer> {
        @Override
        public int compare(TraceAnalyzer t1, TraceAnalyzer t2) {
            return t2.getWeight() - t1.getWeight();
        }
    }

    public void setSupported(GlobalEventIdentifier.EntryType type);
    public GlobalEventIdentifier.EntryType getSupported();

    public void registerParent(TraceReader reader);

    public boolean update(TraceEntry entry);

    public void clear();

    public int getWeight();
    public void setWeight(int weight);

    public String getName();
    public void setName(String name);

    public String toJSON();
    public void writeJSON(OutputStream output) throws IOException;

    public String toCSV();
    public void writeCSV(OutputStream output) throws IOException;

    public void setEventNameResolver(EventNameResolver resolver);
}
