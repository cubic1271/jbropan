package edu.ohio.irg.bro.trace.json;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import edu.ohio.irg.bro.trace.state.GlobalEventIdentifier;
import edu.ohio.irg.bro.trace.util.EventNameResolver;

import java.lang.reflect.Type;

public class GlobalEventIdentifierAdapter implements JsonSerializer<GlobalEventIdentifier> {
    protected transient EventNameResolver resolver;

    public GlobalEventIdentifierAdapter(EventNameResolver resolver) {
        this.resolver = resolver;
    }

    @Override
    public JsonElement serialize(GlobalEventIdentifier src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject obj = new JsonObject();
        obj.addProperty("resolved", src.toString());
        obj.addProperty("raw", src.toString());
        return obj;
    }
}
