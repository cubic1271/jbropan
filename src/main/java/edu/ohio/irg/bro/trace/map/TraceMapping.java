package edu.ohio.irg.bro.trace.map;

import java.util.ArrayList;

public class TraceMapping {
    private String endian;
    private ArrayList<FunctionMapping> mappings;
    private ArrayList<ComponentMapping> components;

    public TraceMapping() {
        mappings = new ArrayList<FunctionMapping>();
    }

    public boolean isLittleEndian() {
        return endian.equals("little");
    }

    public ArrayList<FunctionMapping> getMappings() {
        return this.mappings;
    }

    public ArrayList<ComponentMapping> getComponents() {
        return this.components;
    }
}
