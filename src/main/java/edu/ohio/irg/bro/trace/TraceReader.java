package edu.ohio.irg.bro.trace;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import edu.ohio.irg.bro.trace.analysis.*;
import edu.ohio.irg.bro.trace.exception.InvalidTraceException;
import edu.ohio.irg.bro.trace.exception.TraceRuntimeException;
import edu.ohio.irg.bro.trace.map.ComponentMapping;
import edu.ohio.irg.bro.trace.map.FunctionMapping;
import edu.ohio.irg.bro.trace.map.Mapping;
import edu.ohio.irg.bro.trace.map.TraceMapping;
import edu.ohio.irg.bro.trace.state.GlobalEventIdentifier;
import edu.ohio.irg.bro.trace.state.LocalEventIdentifier;
import edu.ohio.irg.bro.trace.util.EventNameResolver;
import org.h2.compress.CompressLZF;

public class TraceReader implements Runnable {
    protected static final String COUNT_ANALYZER_NAME = "DefaultCountAnalyzer";
    protected static final String CALL_CHAIN_ANALYZER_NAME = "DefaultCallChainAnalyzer";
    protected static final String PACKET_CHAIN_ANALYZER_NAME = "DefaultPacketChainAnalyzer";
    protected static final String SCRIPT_TIMING_ANALYZER_NAME = "DefaultScriptTimingAnalyzer";
    protected static final String EVENT_PATH_ANALYZER_NAME = "DefaultEventPathAnalyzer";
    protected static final String PACKET_AGGREGATE_ANALYZER_NAME = "DefaultPacketAggregateAnalyzer";
    protected static final String PACKET_PERPACKET_AGGREGATE_ANALYZER_NAME = "DefaultPerPacketAggregateAnalyzer";
    protected static final String MEMORY_PERPACKET_AGGREGATE_ANALYZER_NAME = "DefaultPerPacketMemoryAggregateAnalyzer";
    protected static final String MEMORY_AGGREGATE_ANALYZER_NAME = "DefaultMemoryAggregateAnalyzer";


    public static final int BRO_LZF_HEADER_SIZE = 16;

    private String mapFilePath;
    private File mapFile;

    private String traceFilePath;
    private File traceFile;

    private String trialId;

    private boolean littleEndian;

    protected ArrayList<TraceAnalyzer> analyzers;

    protected HashMap<GlobalEventIdentifier, Mapping> mappings = new HashMap<GlobalEventIdentifier, Mapping>();

    protected EventNameResolver resolver;

    public TraceReader() {
        analyzers = new ArrayList<TraceAnalyzer>();
        registerAnalyzer(new CountAnalyzer(COUNT_ANALYZER_NAME));
        //registerAnalyzer(new CallChainAnalyzer(CALL_CHAIN_ANALYZER_NAME));
        //registerAnalyzer(new PacketChainAnalyzer(PACKET_CHAIN_ANALYZER_NAME));
        //registerAnalyzer(new ScriptTimingAnalyzer(SCRIPT_TIMING_ANALYZER_NAME));
        //registerAnalyzer(new EventPathAnalyzer(EVENT_PATH_ANALYZER_NAME));
        registerAnalyzer(new PacketAggregateAnalyzer(PACKET_AGGREGATE_ANALYZER_NAME, false));
        registerAnalyzer(new PacketAggregateAnalyzer(PACKET_PERPACKET_AGGREGATE_ANALYZER_NAME, true));
        registerAnalyzer(new MemoryAggregateAnalyzer(MEMORY_AGGREGATE_ANALYZER_NAME, false));
        registerAnalyzer(new MemoryAggregateAnalyzer(MEMORY_PERPACKET_AGGREGATE_ANALYZER_NAME, true));
    }

    public boolean registerAnalyzer(TraceAnalyzer analyzer) {
        for(TraceAnalyzer curr: analyzers) {
            if(curr.getName().equals(analyzer.getName())) {
                return false;
            }
        }
        analyzer.registerParent(this);
        analyzers.add(analyzer);
        Collections.sort(analyzers, new TraceAnalyzer.WeightComparator());
        return true;
    }

    public void resetAnalyzers() {
        for(TraceAnalyzer curr: analyzers) {
            curr.clear();
        }
    }

    public void removeAnalyzer(TraceAnalyzer target) {
        for(TraceAnalyzer curr: analyzers) {
            if(curr.getName().equals(target.getName())) {
                analyzers.remove(curr);
                return;
            }
        }
    }

    public TraceAnalyzer getAnalyzer(String name) {
        for(TraceAnalyzer curr: analyzers) {
            if(curr.getName().equals(name)) {
                return curr;
            }
        }
        return null;
    }

    public void setMapFile(String path) throws IOException {
        this.mapFilePath = path;
        this.mapFile = new File(path);

        if(!this.mapFile.exists()) {
            throw new IOException("No such map file: " + path);
        }

        if(!this.mapFile.canRead()) {
            throw new IOException("Unable to open map file: unable to read file at " + path + "");
        }
    }

    public void openMapFile(String path) throws IOException {
        setMapFile(path);
        openMapFile();
    }

    public void openMapFile() throws IOException {
        mappings = new HashMap<GlobalEventIdentifier, Mapping>();
        FileInputStream mapFileInput = new FileInputStream(mapFile);
        InputStreamReader mapFileReader = new InputStreamReader(mapFileInput);

        Gson gson = new GsonBuilder().create();
        TraceMapping mapping = null;
        try {
            mapping = gson.fromJson(mapFileReader, TraceMapping.class);
        }
        catch(JsonSyntaxException ex) {
            throw new InvalidTraceException("Invalid map file specified", ex);
        }
        if(null == mapping) {
            throw new InvalidTraceException("Unknown error constructing mapping object (mapping == null?!)");
        }
        this.littleEndian = mapping.isLittleEndian();
        for(FunctionMapping map : mapping.getMappings())
        {
            // FIXME: Hack to extract event id / body fields in platform-independent way.
            ByteBuffer buffer = ByteBuffer.allocate(4);
            buffer.order((this.littleEndian) ? ByteOrder.LITTLE_ENDIAN : ByteOrder.BIG_ENDIAN);
            buffer.putInt(map.getId());
            buffer.rewind();
            Short tmpId = 0;
            Short tmpBody = 0;
            if(buffer.order() == ByteOrder.LITTLE_ENDIAN) {
                tmpId = buffer.getShort();
                tmpBody = buffer.getShort();
            }
            else {
                tmpBody = buffer.getShort();
                tmpId = buffer.getShort();
            }
            mappings.put(new GlobalEventIdentifier(new LocalEventIdentifier(tmpId, tmpBody), GlobalEventIdentifier.EntryType.SCRIPT), map);
        }
        for(ComponentMapping map : mapping.getComponents())
        {
            // FIXME: Hack to extract event id / body fields in platform-independent way.
            ByteBuffer buffer = ByteBuffer.allocate(4);
            buffer.order((this.littleEndian) ? ByteOrder.LITTLE_ENDIAN : ByteOrder.BIG_ENDIAN);
            buffer.putInt(map.getId());
            buffer.rewind();
            Short tmpId = 0;
            Short tmpBody = 0;
            if(buffer.order() == ByteOrder.LITTLE_ENDIAN) {
                tmpId = buffer.getShort();
                tmpBody = buffer.getShort();
            }
            else {
                tmpBody = buffer.getShort();
                tmpId = buffer.getShort();
            }
            mappings.put(new GlobalEventIdentifier(new LocalEventIdentifier(tmpId, tmpBody), GlobalEventIdentifier.EntryType.COMPONENT), map);
        }

        this.resolver = new EventNameResolver();
        this.resolver.setMapping(mappings);

        for(TraceAnalyzer analyzer : analyzers) {
            analyzer.setEventNameResolver(this.resolver);
        }
    }

    public void setTraceFile(String path) throws IOException {
        this.traceFilePath = path;
        this.traceFile = new File(path);

        if(!this.traceFile.exists()) {
            throw new IOException("No such map file: " + path);
        }

        if(!this.traceFile.canRead()) {
            throw new IOException("Unable to open map file: unable to read file at " + path + "");
        }
    }

    public void openTraceFile(String path) throws IOException {
        setTraceFile(path);
        openTraceFile();
    }

    public void openTraceFile() throws IOException {
        if(null == mapFile) {
            throw new InvalidTraceException("A valid map file must be loaded before a trace file may be processed.");
        }

        FileInputStream traceFileInput = new FileInputStream(traceFile);
        FileChannel channel = traceFileInput.getChannel();

        ByteBuffer blockHeader = ByteBuffer.allocate(BRO_LZF_HEADER_SIZE);
        blockHeader.order(isLittleEndian() ? ByteOrder.LITTLE_ENDIAN : ByteOrder.BIG_ENDIAN);

        CompressLZF lzfHandler = new CompressLZF();

        long virtualBase = 0;
        long realBase = 0;

        boolean isFirstEntry = true;
        int bytesRead = 0;

        while(bytesRead >= 0) {
            // need to reset the buffer so we can do a read
            blockHeader.rewind();
            bytesRead = channel.read(blockHeader);
            // and rewind *again* so that we can retrieve the data we just read
            blockHeader.rewind();

            if(bytesRead < 0) {
                continue;
            }

            int uncompressedSize = blockHeader.getInt();
            int compressedSize = blockHeader.getInt();
            int numEntries = blockHeader.getInt();
            int version = blockHeader.getInt();

            if(version != 1) {
                throw new InvalidTraceException("Read block with invalid version field: " + version);
            }

            // System.out.println("Read block: " + uncompressedSize + " / " + compressedSize + " (" + numEntries + ")");

            ByteBuffer readBuffer = ByteBuffer.allocate(compressedSize);
            readBuffer.order((isLittleEndian()) ? ByteOrder.LITTLE_ENDIAN : ByteOrder.BIG_ENDIAN);
            ByteBuffer targetBuffer = null;

            bytesRead = channel.read(readBuffer);
            if(bytesRead < 0) {
                continue;
            }
            readBuffer.rewind();

            // System.out.println("Read " + bytesRead + " bytes.");

            if(compressedSize < uncompressedSize) {
                targetBuffer = ByteBuffer.allocate(uncompressedSize);
                // NOTE: array LZF decompression is much faster, so that's the way we go here.
                byte[] inArray = readBuffer.array();
                byte[] outArray = targetBuffer.array();
                lzfHandler.expand(inArray, 0, compressedSize, outArray, 0, uncompressedSize);
                // lzfHandler.expand(readBuffer, targetBuffer);
                targetBuffer.rewind();
            }
            else if(compressedSize == uncompressedSize) {
                targetBuffer = readBuffer;
                targetBuffer.rewind();
            }
            else {
                throw new InvalidTraceException("Bad block: compressed size is *GREATER THAN* uncompressed size!  ( " + uncompressedSize + " / " + compressedSize + " )");
            }

            int entryCount = 0;

            targetBuffer.order((isLittleEndian()) ? ByteOrder.LITTLE_ENDIAN : ByteOrder.BIG_ENDIAN);
            try {
                while(targetBuffer.hasRemaining()) {
                    TraceEntry entry = new TraceEntry();
                    entry.populate(targetBuffer);
                    if(isFirstEntry) {
                        virtualBase = entry.getPapiVirtual();
                        isFirstEntry = false;
                    }
                    entry.adjustPapiTimestamps(virtualBase);
                    boolean doAnalysis = true;
                    for(TraceAnalyzer analyzer: analyzers) {
                        if(doAnalysis) {
                            // System.out.println("Executing analyzer: " + analyzer.getName() + " :: " + analyzer.getWeight());
                            doAnalysis = analyzer.update(entry);
                        }
                    }
                    ++entryCount;
                }
            }
            finally {
                //
            }

            if(entryCount != numEntries) {
                throw new InvalidTraceException("Expecting " + Integer.toString(numEntries) + " entries in block but actually read " + Integer.toString(entryCount));
            }
        }
    }

    public Integer size() {
        return (resolver != null) ? resolver.size() : 0;
    }

    public String resolveName(GlobalEventIdentifier entry) {
        return resolver.has(entry) ? resolver.getName(entry) : null;
    }

    public String resolveName(LocalEventIdentifier id, GlobalEventIdentifier.EntryType type) {
        return resolveName(new GlobalEventIdentifier(id, type));
    }

    public boolean isLittleEndian() {
        return littleEndian;
    }

    public EventNameResolver getResolver() {
        return new EventNameResolver(resolver);
    }

    public CountAnalyzer getCountAnalyzer() {
        return (CountAnalyzer)getAnalyzer(TraceReader.COUNT_ANALYZER_NAME);
    }

    public CallChainAnalyzer getCallChainAnalyzer() {
        return (CallChainAnalyzer)getAnalyzer(TraceReader.CALL_CHAIN_ANALYZER_NAME);
    }

    public PacketChainAnalyzer getPacketChainAnalyzer() {
        return (PacketChainAnalyzer)getAnalyzer(TraceReader.PACKET_CHAIN_ANALYZER_NAME);
    }

    public ScriptTimingAnalyzer getScriptTimingAnalyzer() {
        return (ScriptTimingAnalyzer)getAnalyzer(TraceReader.SCRIPT_TIMING_ANALYZER_NAME);
    }

    public EventPathAnalyzer getEventPathAnalyzer() {
        return (EventPathAnalyzer)getAnalyzer(TraceReader.EVENT_PATH_ANALYZER_NAME);
    }

    public PacketAggregateAnalyzer getPacketAggregateAnalyzer() {
        return (PacketAggregateAnalyzer)getAnalyzer(TraceReader.PACKET_AGGREGATE_ANALYZER_NAME);
    }

    public PacketAggregateAnalyzer getPacketPerPacketAggregateAnalyzer() {
        return (PacketAggregateAnalyzer)getAnalyzer(TraceReader.PACKET_PERPACKET_AGGREGATE_ANALYZER_NAME);
    }

    public MemoryAggregateAnalyzer getMemoryAggregateAnalyzer() {
        return (MemoryAggregateAnalyzer)getAnalyzer(TraceReader.MEMORY_AGGREGATE_ANALYZER_NAME);
    }

    public MemoryAggregateAnalyzer getMemoryPerPacketAggregateAnalyzer() {
        return (MemoryAggregateAnalyzer)getAnalyzer(TraceReader.MEMORY_PERPACKET_AGGREGATE_ANALYZER_NAME);
    }

    @Override
    public void run() {
        if(mapFile == null) {
            throw new TraceRuntimeException("Map file must be set before launching reader.  Aborting run...");
        }
        if(traceFile == null) {
            throw new TraceRuntimeException("Trace file must be set before launching reader.  Aborting run...");
        }

        try {
            openMapFile();
        }
        catch(IOException ex) {
            throw new TraceRuntimeException("Error processing map file.", ex);
        }
        try {
            openTraceFile();
        }
        catch(IOException ex) {
            throw new TraceRuntimeException("Error processing trace file.", ex);
        }
    }

    public String getTrialId() {
        return this.trialId;
    }

    public void setTrialId(String trialId) {
        this.trialId = trialId;
    }

}
