package edu.ohio.irg.bro.trace.analysis.timing;

import edu.ohio.irg.bro.trace.state.GlobalEventIdentifier;

import java.util.HashMap;

public class EventTimingContainer {
    protected HashMap<GlobalEventIdentifier, EventTimingAccumulator> timings;

    public EventTimingContainer() {
        timings = new HashMap<GlobalEventIdentifier, EventTimingAccumulator>();
    }

    public void clear() {
        timings.clear();
    }

    public int size() {
        return timings.size();
    }

    public void accumulate(GlobalEventIdentifier id, EventTiming curr, EventTiming base) {
        if(!timings.containsKey(id)) {
            timings.put(id, new EventTimingAccumulator());
        }
        timings.get(id).accumulate(curr, base);
    }

    public HashMap<GlobalEventIdentifier, EventTimingAccumulator> getTimings() {
        return new HashMap<GlobalEventIdentifier, EventTimingAccumulator>(timings);
    }
}
