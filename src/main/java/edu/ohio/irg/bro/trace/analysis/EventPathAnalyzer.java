package edu.ohio.irg.bro.trace.analysis;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import edu.ohio.irg.bro.trace.TraceEntry;
import edu.ohio.irg.bro.trace.analysis.graph.EventPathGraph;
import edu.ohio.irg.bro.trace.json.EventPathAnalyzerAdapter;
import edu.ohio.irg.bro.trace.state.GlobalEventIdentifier;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Stack;

public class EventPathAnalyzer extends BasicAnalyzer {
    public static final int DEFAULT_WEIGHT = 1000;
    public static final String DEFAULT_NAME = "EventPathAnalyzer";

    protected Stack<GlobalEventIdentifier> scriptStack = new Stack<GlobalEventIdentifier>();

    protected EventPathGraph eventGraph = new EventPathGraph();

    public EventPathAnalyzer() {
        this.weight = DEFAULT_WEIGHT;
        this.name = DEFAULT_NAME;
    }

    public EventPathAnalyzer(int weight) {
        this.weight = weight;
        this.name = DEFAULT_NAME;
    }

    public EventPathAnalyzer(String name) {
        this.weight = DEFAULT_WEIGHT;
        this.name = name;
    }

    public EventPathAnalyzer(int weight, String name) {
        this.weight = weight;
        this.name = name;
    }

    @Override
    public boolean update(TraceEntry entry) {
        super.update(entry);
        GlobalEventIdentifier currId = entry.getGlobalId();
        if(entry.isCall()) {
            if(scriptStack.empty()) {
                // new script root
            }
            else {
                // calling from an existing script source
                if(!eventGraph.contains(scriptStack.peek(), currId)) {
                    eventGraph.add(scriptStack.peek(), currId);
                }
                eventGraph.increment(scriptStack.peek(), currId);
            }
            scriptStack.push(currId);
        }
        else if(entry.isReturn()) {
            if(scriptStack.empty()) {
                return true;
            }
            if(! scriptStack.peek().equals(currId)) {
                return true;
            }
            scriptStack.pop();
        }
        return true;
    }

    public Integer getCount(GlobalEventIdentifier source, GlobalEventIdentifier sink) {
        return eventGraph.getCount(source, sink);
    }

    @Override
    public void clear() {
        super.clear();
        scriptStack.clear();
        eventGraph.clear();
    }

    public EventPathGraph getEventGraph() {
        return eventGraph;
    }

    @Override
    public String toString() {
        return "EventPathAnalyzer built " + eventGraph.getVertices().size() + " vertices and processed " + counter + " entries.";
    }

    @Override
    public String toJSON() {
            Gson gson = new GsonBuilder()
                    .registerTypeAdapter(EventPathAnalyzer.class, new EventPathAnalyzerAdapter(getParent().getResolver()))
                    .create();
            return gson.toJson(this);
    }
}
