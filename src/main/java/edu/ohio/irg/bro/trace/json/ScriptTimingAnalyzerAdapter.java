package edu.ohio.irg.bro.trace.json;

import com.google.gson.*;
import edu.ohio.irg.bro.trace.analysis.CallChainAnalyzer;
import edu.ohio.irg.bro.trace.analysis.ScriptTimingAnalyzer;
import edu.ohio.irg.bro.trace.analysis.timing.EventTiming;
import edu.ohio.irg.bro.trace.analysis.timing.EventTimingAccumulator;
import edu.ohio.irg.bro.trace.state.ChainContainer;
import edu.ohio.irg.bro.trace.state.GlobalEventIdentifier;
import edu.ohio.irg.bro.trace.util.EventNameResolver;

import java.lang.reflect.Type;
import java.util.HashMap;

public class ScriptTimingAnalyzerAdapter implements JsonSerializer<ScriptTimingAnalyzer> {
    protected transient EventNameResolver resolver;

    public ScriptTimingAnalyzerAdapter(EventNameResolver resolver) {
        this.resolver = resolver;
    }

    @Override
    public JsonElement serialize(ScriptTimingAnalyzer src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject obj = new JsonObject();
        obj.addProperty("count", src.getCounter());
        obj.addProperty("trial_id", src.getParent().getTrialId());

        HashMap<GlobalEventIdentifier, EventTimingAccumulator> timings = src.getTimings().getTimings();

        JsonObject accum = new JsonObject();

        for(GlobalEventIdentifier curr : timings.keySet()) {
            JsonObject entry = new JsonObject();
            EventTimingAccumulator accumulator = timings.get(curr);
            JsonObject sumObject = new JsonObject();

            for(int i = 0; i < EventTiming.TIME_NUM_COUNTERS; ++i) {
                sumObject.addProperty(EventTiming.TIME_FIELD_NAMES[i], accumulator.getTotal(i));
            }
            entry.add("sum", sumObject);
            JsonObject averageObject = new JsonObject();
            for(int i = 0; i < EventTiming.TIME_NUM_COUNTERS; ++i) {
                averageObject.addProperty(EventTiming.TIME_FIELD_NAMES[i], accumulator.getAverage(i));
            }
            entry.add("average", averageObject);
            JsonObject varianceObject = new JsonObject();
            for(int i = 0; i < EventTiming.TIME_NUM_COUNTERS; ++i) {
                varianceObject.addProperty(EventTiming.TIME_FIELD_NAMES[i], accumulator.getVariance(i));
            }
            entry.add("variance", varianceObject);

            entry.addProperty("count", accumulator.getCount());

            accum.add(curr.toString(), entry);
        }

        obj.add("stats", accum);

        return obj;
    }
}
