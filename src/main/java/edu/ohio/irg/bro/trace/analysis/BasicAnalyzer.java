package edu.ohio.irg.bro.trace.analysis;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import edu.ohio.irg.bro.trace.TraceEntry;
import edu.ohio.irg.bro.trace.TraceReader;
import edu.ohio.irg.bro.trace.json.GlobalEventIdentifierAdapter;
import edu.ohio.irg.bro.trace.state.GlobalEventIdentifier;
import edu.ohio.irg.bro.trace.util.EventNameResolver;

import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;

public class BasicAnalyzer implements TraceAnalyzer {

    public static final int DEFAULT_WEIGHT = 100;
    public static final String DEFAULT_NAME = BasicAnalyzer.class.getName();

    private transient TraceReader parent = null;

    protected int weight;
    protected String name;

    protected EventNameResolver resolver;
    protected GlobalEventIdentifier.EntryType supported = GlobalEventIdentifier.EntryType.ALL;
    protected long counter = 0;

    public BasicAnalyzer() {
        this.weight = DEFAULT_WEIGHT;
        this.name = DEFAULT_NAME;
    }

    public BasicAnalyzer(int weight) {
        this.weight = weight;
        this.name = DEFAULT_NAME;
    }

    public BasicAnalyzer(String name) {
        this.weight = DEFAULT_WEIGHT;
        this.name = name;
    }

    public BasicAnalyzer(int weight, String name) {
        this.weight = weight;
        this.name = name;
    }

    public long getCounter() {
        return counter;
    }

    public boolean isSupported(TraceEntry entry) {
        return (entry.isFunction() && (supported == GlobalEventIdentifier.EntryType.ALL || supported == GlobalEventIdentifier.EntryType.SCRIPT))
                || (entry.isComponent() && (supported == GlobalEventIdentifier.EntryType.ALL || supported == GlobalEventIdentifier.EntryType.COMPONENT));
    }

    public TraceReader getParent() {
        return this.parent;
    }

    @Override
    public int getWeight() {
        return this.weight;
    }

    @Override
    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void registerParent(TraceReader reader) {
        this.parent = reader;
    }

    @Override
    public boolean update(TraceEntry entry) {
        ++counter;
        return true;
    }

    @Override
    public void clear() {
        counter = 0;
    }

    @Override
    public GlobalEventIdentifier.EntryType getSupported() {
        return supported;
    }

    @Override
    public void setSupported(GlobalEventIdentifier.EntryType supported) {
        this.supported = supported;
    }

    @Override
    public String toJSON() {
        Gson gson = new GsonBuilder()
                .create();
        return gson.toJson(this);
    }

    @Override
    public void writeJSON(OutputStream output) throws IOException {
        Gson gson = new GsonBuilder()
                .create();
        output.write(gson.toJson(this).getBytes());
    }

    @Override
    public String toCSV() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void writeCSV(OutputStream output) throws IOException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setEventNameResolver(EventNameResolver resolver) {
        this.resolver = resolver;
    }
}
