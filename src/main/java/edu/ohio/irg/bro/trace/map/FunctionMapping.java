package edu.ohio.irg.bro.trace.map;

import java.util.ArrayList;
import java.util.Stack;

public class FunctionMapping implements Mapping {

    private ArrayList<BodyMapping> bodies;
    private String filename;
    private Integer firstColumn;
    private Integer firstLine;
    private Integer id;
    private Integer lastLine;
    private Integer lastColumn;
    private String name;
    private boolean builtin;
    private boolean hook;
    private boolean event;

    public static String getRelativeScriptName(String script) {
        String[] pathArray = script.split("/");
        ArrayList<String> relativeName = new ArrayList<String>();
        boolean seenScriptsDirectory = false;
        for(int i = 0; i < pathArray.length; ++i) {
            if(seenScriptsDirectory && !pathArray[i].equals(".")) {
                relativeName.add(pathArray[i]);
            }
            if(pathArray[i].equals("share")) {
                seenScriptsDirectory = true;
            }
        }
        StringBuilder pathBuilder = new StringBuilder();
        for(String entry: relativeName) {
            pathBuilder.append("/");
            pathBuilder.append(entry);
        }

        return pathBuilder.toString();
    }

    public FunctionMapping() {
        bodies = new ArrayList<BodyMapping>();
    }

    public Integer size() {
        return bodies.size();
    }

    public ArrayList<BodyMapping> getBodies() {
        return new ArrayList<BodyMapping>(bodies);
    }

    public BodyMapping getBodyById(int id) {
        for(BodyMapping curr : bodies) {
            // System.out.println(curr.filename + " ( " + curr.getId() + " / " + this.size() + ")");
            if(curr.getId().equals(id)) {
                return curr;
            }
        }
        return null;
    }

    @Override
    public String getName() {
        return name;
    }

    public String getModuleName() {
        String[] nameComponents = this.name.split("::");
        if(nameComponents.length < 2) {
            return "GLOBAL";
        }
        return nameComponents[0];
    }

    public String getLocalName() {
        String[] nameComponents = this.name.split("::");
        if(nameComponents.length < 2) {
            return this.name;
        }
        return nameComponents[1];
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Integer getFirstLine() {
        return firstLine;
    }

    public void setFirstLine(Integer firstLine) {
        this.firstLine = firstLine;
    }

    public Integer getLastLine() {
        return lastLine;
    }

    public void setLastLine(Integer lastLine) {
        this.lastLine = lastLine;
    }

    public Integer getFirstColumn() {
        return firstColumn;
    }

    public void setFirstColumn(Integer firstColumn) {
        this.firstColumn = firstColumn;
    }

    public Integer getLastColumn() {
        return lastColumn;
    }

    public void setLastColumn(Integer lastColumn) {
        this.lastColumn = lastColumn;
    }

    public class BodyMapping {
        private String filename;
        private String firstColumn;
        private String firstLine;
        private String id;
        private String lastLine;
        private String lastColumn;

        public String getFilename() {
            return filename;
        }

        public void setFilename(String filename) {
            this.filename = filename;
        }

        public Integer getFirstLine() {
            return Integer.valueOf(firstLine).intValue();
        }

        public void setFirstLine(Integer firstLine) {
            this.firstLine = "" + firstLine;
        }

        public Integer getLastLine() {
            return Integer.valueOf(lastLine).intValue();
        }

        public void setLastLine(Integer lastLine) {
            this.lastLine = "" + lastLine;
        }

        public Integer getFirstColumn() {
            return Integer.valueOf(firstColumn).intValue();
        }

        public void setFirstColumn(Integer firstColumn) {
            this.firstColumn = "" + firstColumn;
        }

        public Integer getLastColumn() {
            return Integer.valueOf(lastColumn).intValue();
        }

        public void setLastColumn(Integer lastColumn) {
            this.lastColumn = "" + lastColumn;
        }

        public Integer getId() {
            return Integer.valueOf(id).intValue();
        }

        public void setId(Integer id) {
            this.id = "" + id;
        }
    }

    public boolean isBuiltin() {
        return builtin;
    }

    public void setBuiltin(boolean builtin) {
        this.builtin = builtin;
    }

    public boolean isHook() {
        return hook;
    }

    public void setHook(boolean hook) {
        this.hook = hook;
    }

    public boolean isEvent() {
        return event;
    }

    public void setEvent(boolean event) {
        this.event = event;
    }
}
