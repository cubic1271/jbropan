package edu.ohio.irg.bro.trace.state;

import edu.ohio.irg.bro.trace.util.EventNameResolver;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.util.ArrayList;
import java.util.Stack;

public class ChainContainer extends ArrayList<GlobalEventIdentifier> {
    public ChainContainer() {
        super();
    }

    public ChainContainer(ArrayList<GlobalEventIdentifier> copy) {
        for(GlobalEventIdentifier entry : copy) {
            this.add(new GlobalEventIdentifier(entry));
        }
    }

    public ChainContainer(Stack<GlobalEventIdentifier> entries) {
        super();
        this.addAll(entries);
    }

    @Override
    public String toString() {
        String rval = "";
        for(int i = 0; i < this.size(); ++i) {
            rval += this.get(i).toString();
            if(i + 1 < this.size()) {
                rval += ", ";
            }
        }
        return rval;
    }

    public String toString(EventNameResolver resolver) {
        String rval = "";
        for(int i = 0; i < this.size(); ++i) {
            rval += this.get(i).toString(resolver);
            if(i + 1 < this.size()) {
                rval += ", ";
            }
        }
        return rval;
    }

    @Override
    public boolean equals(Object eq) {
        if(eq == null) {
            return false;
        }
        if(eq == this) {
            return true;
        }
        if (!(eq instanceof ChainContainer)) {
            return false;
        }

        ChainContainer entries = (ChainContainer) eq;

        if(this.size() != entries.size()) {
            return false;
        }

        for(int i = 0; i < entries.size(); ++i) {
            if(!entries.get(i).equals(this.get(i))) {
                return false;
            }
        }

        return true;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hc = new HashCodeBuilder();

        for(int i = 0; i < this.size(); ++i) {
            hc.append(this.get(i));
        }

        return hc.toHashCode();
    }
}
