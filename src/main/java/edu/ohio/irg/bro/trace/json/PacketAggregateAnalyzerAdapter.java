package edu.ohio.irg.bro.trace.json;

import com.google.gson.*;
import edu.ohio.irg.bro.trace.analysis.PacketAggregateAnalyzer;
import edu.ohio.irg.bro.trace.util.EventNameResolver;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class PacketAggregateAnalyzerAdapter implements JsonSerializer<PacketAggregateAnalyzer> {
    protected transient EventNameResolver resolver;

    public PacketAggregateAnalyzerAdapter(EventNameResolver resolver) {
        this.resolver = resolver;
    }

    @Override
    public JsonElement serialize(PacketAggregateAnalyzer src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject obj = new JsonObject();
        obj.addProperty("trial_id", src.getParent().getTrialId());

        obj.addProperty("count", src.getCounter());

        ArrayList<Long> counts = src.getTriggerTotals();
        ArrayList<Long> totals = src.getPacketTotals();

        JsonArray countArray = new JsonArray();
        for(Long curr : counts) {
            countArray.add(new JsonPrimitive(curr));
        }
        JsonArray totalArray = new JsonArray();
        for(Long curr : totals) {
            totalArray.add(new JsonPrimitive(curr));
        }
        obj.add("triggerTotals", countArray);
        obj.add("packetTotals", totalArray);
        return obj;
    }
}
