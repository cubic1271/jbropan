package edu.ohio.irg.bro.trace.json;

import com.google.gson.*;
import edu.ohio.irg.bro.trace.analysis.EventPathAnalyzer;
import edu.ohio.irg.bro.trace.analysis.PacketChainAnalyzer;
import edu.ohio.irg.bro.trace.analysis.graph.EventPathEdge;
import edu.ohio.irg.bro.trace.analysis.graph.EventPathGraph;
import edu.ohio.irg.bro.trace.state.ChainContainer;
import edu.ohio.irg.bro.trace.state.GlobalEventIdentifier;
import edu.ohio.irg.bro.trace.util.EventNameResolver;

import java.lang.reflect.Type;
import java.util.HashMap;

public class EventPathAnalyzerAdapter implements JsonSerializer<EventPathAnalyzer> {
    protected transient EventNameResolver resolver;

    public EventPathAnalyzerAdapter(EventNameResolver resolver) {
        this.resolver = resolver;
    }

    @Override
    public JsonElement serialize(EventPathAnalyzer src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject obj = new JsonObject();
        obj.addProperty("count", src.getCounter());
        obj.addProperty("trial_id", src.getParent().getTrialId());

        JsonObject vertexEntries = new JsonObject();

        EventPathGraph graph = src.getEventGraph();
        for(GlobalEventIdentifier source : graph.getVertices()) {
            JsonObject vertexObject = new JsonObject();
            JsonArray edgeArray = new JsonArray();
            for(EventPathEdge edge : graph.getEdges(source)) {
                JsonObject edgeObject = new JsonObject();
                edgeObject.addProperty("called", edge.getSink().toString());
                edgeObject.addProperty("count", edge.getWeight());
                edgeArray.add(edgeObject);
            }
            vertexObject.add("edges", edgeArray);
            vertexEntries.add(source.toString(), vertexObject);
        }
        obj.add("graph", vertexEntries);

        return obj;
    }
}
