package edu.ohio.irg.bro.trace.analysis;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import edu.ohio.irg.bro.trace.TraceEntry;
import edu.ohio.irg.bro.trace.exception.TraceRuntimeException;
import edu.ohio.irg.bro.trace.json.CallChainAnalyzerAdapter;
import edu.ohio.irg.bro.trace.state.ChainContainer;
import edu.ohio.irg.bro.trace.state.GlobalEventIdentifier;
import edu.ohio.irg.bro.trace.util.EventNameResolver;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Stack;

public class CallChainAnalyzer extends BasicAnalyzer {
    public static final int DEFAULT_MAX_CHAIN_SIZE = 200000;
    public static final int DEFAULT_WEIGHT = 100;
    public static final String DEFAULT_NAME = CallChainAnalyzer.class.getName();

    protected Stack<GlobalEventIdentifier> entries = new Stack<GlobalEventIdentifier>();
    protected HashMap<ChainContainer, Integer> chainCounts = new HashMap<ChainContainer, Integer>();
    protected int maxChainSize = DEFAULT_MAX_CHAIN_SIZE;

    public CallChainAnalyzer() {
        this.weight = DEFAULT_WEIGHT;
        this.name = DEFAULT_NAME;
    }

    public CallChainAnalyzer(int weight) {
        this.weight = weight;
        this.name = DEFAULT_NAME;
    }

    public CallChainAnalyzer(String name) {
        this.weight = DEFAULT_WEIGHT;
        this.name = name;
    }

    public CallChainAnalyzer(int weight, String name) {
        this.weight = weight;
        this.name = name;
    }

    void postNewEntryEvent(TraceEntry entry) {

    }

    void preRemoveEntryEvent(TraceEntry entry) {

    }

    @Override
    public boolean update(TraceEntry entry) {
        super.update(entry);
        if(isSupported(entry)) {
            if(entry.isCall()) {
                GlobalEventIdentifier globalEventIdentifier = new GlobalEventIdentifier(entry);
                entries.push(globalEventIdentifier);
                if(entries.size() > maxChainSize) {
                    throw new TraceRuntimeException("Maximum chain size exceeded (" + maxChainSize + ").");
                }
                postNewEntryEvent(entry);
            }
            // Note that if entries.empty(), then we'll end up with an empty call chain.  This is an artifact of enabling
            // tracing in bro after *some* script code has already been run (e.g. the first call actually traced is the
            // return from the call to enable tracing in the first place).
            else if(entry.isReturn() && !entries.empty()) {
                ChainContainer chain = new ChainContainer(entries);
                if(chainCounts.containsKey(chain)) {
                    chainCounts.put(chain, chainCounts.get(chain) + 1);
                }
                else {
                    chainCounts.put(chain, new Integer(1));
                }
                preRemoveEntryEvent(entry);
                entries.pop();
            }
        }
        return true;
    }

    @Override
    public void clear() {
        super.clear();
        chainCounts.clear();
        entries.clear();
    }

    @Override
    public String toString() {
        return "CallChainAnalyzer with " + chainCounts.size() + " unique chains after processing " + counter + " entries.";
    }

    public HashMap<ChainContainer, Integer> getChainCounts() {
        return new HashMap<ChainContainer, Integer>(chainCounts);
    }

    public int getMaxChainSize() {
        return maxChainSize;
    }

    public void setMaxChainSize(int maxChainSize) {
        this.maxChainSize = maxChainSize;
    }

    //TODO: duplicated code from PacketChainAnalyzer.  How can we fix that?
    public HashMap<String, Integer> difference(CallChainAnalyzer target, EventNameResolver resolver) {
        HashMap<String, Integer> difference = new HashMap<String, Integer>();
        // check to see what we have that the other analyzer doesn't ...
        for(ChainContainer container : chainCounts.keySet()) {
            if(target.chainCounts.containsKey(container.toString(resolver))) {
                difference.put(container.toString(resolver), chainCounts.get(container) - target.chainCounts.get(container));
            }
            else {
                difference.put(container.toString(resolver), chainCounts.get(container));
            }
        }

        // check to see what the other analyzer has that we don't ...
        // insert that into our difference map as negative values
        for(ChainContainer container : target.chainCounts.keySet()) {
            if(!chainCounts.containsKey(container)) {
                difference.put(container.toString(resolver), -target.chainCounts.get(container));
            }
        }

        return difference;
    }

    @Override
    public String toJSON() {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(CallChainAnalyzer.class, new CallChainAnalyzerAdapter(getParent().getResolver()))
                .create();
        return gson.toJson(this);
    }
}
