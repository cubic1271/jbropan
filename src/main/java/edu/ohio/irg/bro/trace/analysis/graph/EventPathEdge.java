package edu.ohio.irg.bro.trace.analysis.graph;

import edu.ohio.irg.bro.trace.state.GlobalEventIdentifier;

public class EventPathEdge {
    protected GlobalEventIdentifier source;
    protected GlobalEventIdentifier sink;
    protected Double weight;

    public EventPathEdge(GlobalEventIdentifier source, GlobalEventIdentifier sink) {
        this.source = source;
        this.sink = sink;
        this.weight = 0.0;
    }

    public GlobalEventIdentifier getSource() {
        return new GlobalEventIdentifier(source);
    }

    public GlobalEventIdentifier getSink() {
        return new GlobalEventIdentifier(sink);
    }

    public void add(Double value) {
        this.weight += value;
    }

    public void increment() {
        this.weight++;
    }

    public void clear() {
        this.weight = 0.0;
    }

    public Double getWeight() {
        return this.weight;
    }
}
