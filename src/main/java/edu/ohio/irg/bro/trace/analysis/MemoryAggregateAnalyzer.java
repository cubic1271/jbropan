package edu.ohio.irg.bro.trace.analysis;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import edu.ohio.irg.bro.trace.TraceEntry;
import edu.ohio.irg.bro.trace.json.PacketAggregateAnalyzerAdapter;
import edu.ohio.irg.bro.trace.map.FunctionMapping;
import edu.ohio.irg.bro.trace.state.MemoryContainer;
import edu.ohio.irg.bro.trace.util.EventNameResolver;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Stack;

// TODO: refactor this!  So many arrays and hashmaps :(
public class MemoryAggregateAnalyzer extends BasicAnalyzer {
    protected ArrayList<MemoryContainer> scriptTotals = new ArrayList<MemoryContainer>();
    protected ArrayList<HashMap<String, MemoryContainer>> functionTotals = new ArrayList<HashMap<String, MemoryContainer>>();

    protected HashSet<String> uniqueFileList = new HashSet<String>();

    protected HashMap<String, Stack<MemoryContainer>> functionStart = new HashMap<String, Stack<MemoryContainer>>();
    protected HashMap<String, MemoryContainer> functionSum = new HashMap<String, MemoryContainer>();
    protected Stack<MemoryContainer> scriptStart = new Stack<MemoryContainer>();
    protected MemoryContainer scriptSum = new MemoryContainer();

    protected boolean perPacketCounters = true;
    public static final int DEFAULT_WEIGHT = 1000;
    public static final String DEFAULT_NAME = "MemoryAggregateAnalyzer";

    public MemoryAggregateAnalyzer() {
        this.weight = DEFAULT_WEIGHT;
        this.name = DEFAULT_NAME;
    }

    public MemoryAggregateAnalyzer(int weight) {
        this.weight = weight;
        this.name = DEFAULT_NAME;
    }

    public MemoryAggregateAnalyzer(String name) {
        this.weight = DEFAULT_WEIGHT;
        this.name = name;
    }

    public MemoryAggregateAnalyzer(String name, boolean perPacket) {
        this.weight = DEFAULT_WEIGHT;
        this.perPacketCounters = perPacket;
        this.name = name;
    }

    public MemoryAggregateAnalyzer(int weight, String name) {
        this.weight = weight;
        this.name = name;
    }

    @Override
    public boolean update(TraceEntry entry) {
        super.update(entry);
        if(entry.isComponent()) {
            if(entry.getGlobalId().getId().getFunctionId() == TraceEntry.ID_BEGIN_PACKET) {
                // start of the current packet.  record triggerTotals for this packet.
                functionStart = new HashMap<String, Stack<MemoryContainer>>();
                scriptStart = new Stack<MemoryContainer>();

                if(perPacketCounters) {
                    functionSum = new HashMap<String, MemoryContainer>();
                    scriptSum = new MemoryContainer();
                }
            }
            else if(entry.getGlobalId().getId().getFunctionId() == TraceEntry.ID_END_PACKET) {
                scriptTotals.add(scriptSum);
                functionTotals.add(new HashMap<String, MemoryContainer>(functionSum));
            }
        }
        else {
            if(entry.isCall()) {
                scriptStart.push(new MemoryContainer(entry));
                FunctionMapping.BodyMapping mapping = resolver.getBodyInfo(entry.getId());
                if(null == mapping) {
                    // If we have a function entry with no explicit bodies, generate one and give it an ID of 0.
                    FunctionMapping tmpMap = resolver.getFunctionInfo(entry.getId());
                    if(null != tmpMap) {
                        mapping = tmpMap.new BodyMapping();
                        mapping.setFilename(tmpMap.getFilename());
                        mapping.setId(0);
                    }
                }
                if(null != mapping) {
                    String source = mapping.getFilename();
                    if (null != source) {
                        source = FunctionMapping.getRelativeScriptName(source);
                        if( !functionStart.containsKey(source) ) {
                            Stack<MemoryContainer> stack = new Stack<MemoryContainer>();
                            stack.push(new MemoryContainer(entry));
                            functionStart.put(source, stack);
                        }
                        else {
                            functionStart.get(source).push(new MemoryContainer(entry));
                        }
                    }
                    else {
                        System.err.println("[WARN] unable to resolve filename for function: " + entry.getId());
                    }
                    // System.out.println("[INFO] Resolved: " + entry.getId());
                }
                else {
                    System.err.println("[WARN] unable to resolve function: " + entry.getId());
                }
            }
            else if(entry.isReturn()) {
                assert(resolver != null);
                // we're returning from the last frame on the stack, so compute time *only* from this frame.
                if(scriptStart.size() == 1) {
                    // sum += (entry - start)
                    scriptSum = scriptSum.sum(new MemoryContainer(entry).diff(scriptStart.peek()));

                }
                if(scriptStart.size() > 0) {
                    scriptStart.pop();
                }
                FunctionMapping.BodyMapping mapping = resolver.getBodyInfo(entry.getId());
                if(null != mapping) {
                    String source = mapping.getFilename();
                    if(null != source) {
                        source = FunctionMapping.getRelativeScriptName(source);
                        if(!uniqueFileList.contains(source)) {
                            uniqueFileList.add(source);
                        }
                        if(!functionStart.containsKey(source)) {
                            // saw a return for a function we hadn't called, so no real good frame of reference here.  Thus, discard.
                            return true;
                        }

                        if(functionSum.containsKey(source)) {
                            // incremental addition since we've seen this function multiple times for this packet
                            functionSum.put(source, (new MemoryContainer(entry).diff(functionStart.get(source).peek())).sum(functionSum.get(source)));
                            functionStart.get(source).pop();
                        }
                        else {
                            // first time we've seen this function for this packet
                            functionSum.put(source, new MemoryContainer(entry).diff(functionStart.get(source).peek()));
                            functionStart.get(source).pop();
                        }

                    }

                }
            }
        }
        return true;
    }

    @Override
    public void clear() {
        super.clear();
        functionTotals.clear();
        functionSum.clear();
        functionStart.clear();

        scriptStart.clear();
        scriptTotals.clear();
        scriptSum = new MemoryContainer();

        uniqueFileList.clear();
    }

    public String toString() {
        return "MemoryAggregateAnalyzer with " + functionTotals.size() + " unique malloc call totals calculated across " + counter + " entries.";
    }

    public HashMap<String, Integer> difference(CountAnalyzer target, EventNameResolver resolver) {
        return null;
    }

    @Override
    public String toJSON() {
        Gson gson = new GsonBuilder()
                .create();
        return gson.toJson(this);
    }

    @Override
    public String toCSV() {
         return "";
    }

    @Override
    public void writeCSV(OutputStream output) throws IOException {
        OutputStreamWriter writer = new OutputStreamWriter(output);

        System.err.println("Writing CSV ... (" + functionTotals.size() + " lines)");
        writer.write("# Aggregate output format.  Output is raw virtual count.\n");
        writer.write("# pktid,script-malloc-bytes,script-malloc-count,script-free-count");
        for(String curr : uniqueFileList) {
            writer.write("," + curr + "-malloc-bytes");
            writer.write("," + curr + "-malloc-count");
            writer.write("," + curr + "-free-count");
        }
        writer.write("\n");

        for(int i = 0; i < functionTotals.size(); ++i) {
            writer.write("" + i);
            MemoryContainer stmp = scriptTotals.get(i);
            writer.write("," + stmp.getMallocSize() + "," + stmp.getMallocCalls() + "," + stmp.getFreeCalls());
            for(String curr : uniqueFileList) {
                if(functionTotals.get(i).containsKey(curr)) {
                    MemoryContainer ftmp = functionTotals.get(i).get(curr);
                    writer.write("," + ftmp.getMallocSize() + "," + ftmp.getMallocCalls() + "," + ftmp.getFreeCalls());
                }
                else {
                    writer.write(",-,-,-");
                }
            }
            writer.write("\n");
        }
        writer.flush();
        writer.close();
    }
}
