package edu.ohio.irg.bro.trace.analysis;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import edu.ohio.irg.bro.trace.TraceEntry;
import edu.ohio.irg.bro.trace.json.CountAnalyzerAdapter;
import edu.ohio.irg.bro.trace.state.GlobalEventIdentifier;
import edu.ohio.irg.bro.trace.util.EventNameResolver;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;

public class CountAnalyzer extends BasicAnalyzer {
    protected HashMap<GlobalEventIdentifier, Integer> counts = new HashMap<GlobalEventIdentifier, Integer>();

    public static final int DEFAULT_WEIGHT = 1000;
    public static final String DEFAULT_NAME = "CountAnalyzer";

    public CountAnalyzer() {
        this.weight = DEFAULT_WEIGHT;
        this.name = DEFAULT_NAME;
    }

    public CountAnalyzer(int weight) {
        this.weight = weight;
        this.name = DEFAULT_NAME;
    }

    public CountAnalyzer(String name) {
        this.weight = DEFAULT_WEIGHT;
        this.name = name;
    }

    public CountAnalyzer(int weight, String name) {
        this.weight = weight;
        this.name = name;
    }

    @Override
    public boolean update(TraceEntry entry) {
        super.update(entry);
        counts.put(entry.getGlobalId(), counts.containsKey(entry.getGlobalId()) ? counts.get(entry.getGlobalId()) + 1: new Integer(1));
        return true;
    }

    @Override
    public void clear() {
        super.clear();
        counts.clear();
    }

    public HashMap<GlobalEventIdentifier, Integer> getCounts() {
        return new HashMap<GlobalEventIdentifier, Integer>(counts);
    }

    public String toString() {
        return "CountAnalyzer with " + counts.size() + " unique triggerTotals calculated across " + counter + " entries.";
    }

    public HashMap<String, Integer> difference(CountAnalyzer target, EventNameResolver resolver) {
        HashMap<String, Integer> difference = new HashMap<String, Integer>();
        // check to see what we have that the other analyzer doesn't ...
        for(GlobalEventIdentifier currEvent : counts.keySet()) {
            if(target.counts.containsKey(currEvent.toString(resolver))) {
                difference.put(currEvent.toString(resolver), counts.get(currEvent) - target.counts.get(currEvent));
            }
            else {
                difference.put(currEvent.toString(resolver), counts.get(currEvent));
            }
        }

        // check to see what the other analyzer has that we don't ...
        // insert that into our difference map as negative values
        for(GlobalEventIdentifier currEvent : target.counts.keySet()) {
            if(!counts.containsKey(currEvent)) {
                difference.put(currEvent.toString(resolver), -target.counts.get(currEvent));
            }
        }

        return difference;
    }

    @Override
    public String toJSON() {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(CountAnalyzer.class, new CountAnalyzerAdapter(getParent().getResolver()))
                .create();
        return gson.toJson(this);
    }
}
