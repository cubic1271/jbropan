package edu.ohio.irg.bro.trace.exception;

public class TraceRuntimeException extends RuntimeException {
    public TraceRuntimeException() {
        super();
    }

    public TraceRuntimeException(String message) {
        super(message);
    }

    public TraceRuntimeException(Exception ex) {
        super(ex);
    }

    public TraceRuntimeException(String message, Exception ex) {
        super(message, ex);
    }
}
