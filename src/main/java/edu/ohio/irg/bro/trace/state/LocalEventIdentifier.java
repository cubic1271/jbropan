package edu.ohio.irg.bro.trace.state;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class LocalEventIdentifier
{
    private Integer functionId;
    private Integer bodyId;

    public LocalEventIdentifier(LocalEventIdentifier copy) {
        this.functionId = new Integer(copy.functionId);
        this.bodyId = new Integer(copy.bodyId);
    }

    public LocalEventIdentifier(Integer functionId, Integer bodyId) {
        this.functionId = new Integer(functionId);
        this.bodyId = new Integer(bodyId);
    }

    public LocalEventIdentifier(Short functionId, Short bodyId) {
        this.functionId = new Integer(functionId & 0xffff);
        this.bodyId = new Integer(bodyId & 0xffff);
    }

    public Integer getBodyId() {
        return new Integer(this.bodyId);
    }

    public Integer getFunctionId() {
        return new Integer(this.functionId);
    }

    @Override
    public String toString() {
        return this.functionId + "." + this.bodyId;
    }

    @Override
    public boolean equals(Object eq) {
        if(eq == null) {
            return false;
        }
        if(eq == this) {
            return true;
        }
        if (!(eq instanceof LocalEventIdentifier)) {
            return false;
        }

        LocalEventIdentifier eqId = (LocalEventIdentifier)eq;

        return     eqId.bodyId.equals(this.bodyId)
                && eqId.functionId.equals(this.functionId);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(41, 849)
                .append(this.functionId)
                .append(this.bodyId)
                .toHashCode();
    }
}
