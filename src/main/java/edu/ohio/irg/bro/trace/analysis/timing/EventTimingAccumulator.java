package edu.ohio.irg.bro.trace.analysis.timing;

public class EventTimingAccumulator {
    protected int count;
    protected double[] sum;
    protected double[] average;
    protected double[] variance;

    public EventTimingAccumulator() {
        sum      = new double[EventTiming.TIME_NUM_COUNTERS];
        average  = new double[EventTiming.TIME_NUM_COUNTERS];
        variance = new double[EventTiming.TIME_NUM_COUNTERS];
        clear();
    }

    public double getTotal(int index) {
        return sum[index];
    }

    public double getAverage(int index) {
        return average[index];
    }

    public double getVariance(int index) {
        return (count <= 1) ? 0 : variance[index] / (count - 1);
    }

    public int getCount() {
        return count;
    }

    public void clear() {
        for(int i = 0; i < EventTiming.TIME_NUM_COUNTERS; ++i) {
            sum[i] = 0;
            average[i] = 0;
            variance[i] = 0;
        }
        count = 0;
    }

    public void accumulate(EventTiming curr, EventTiming base) {
        ++count;
        long[] timings = base.elapsed(curr);  //
        for(int i = 0; i < EventTiming.TIME_NUM_COUNTERS; ++i) {
            sum[i]     += timings[i];
            double delta = timings[i] - average[i];
            average[i]  = average[i] + (delta / (double)count);
            variance[i] = variance[i] + delta * (timings[i] - average[i]);
        }
    }
}
