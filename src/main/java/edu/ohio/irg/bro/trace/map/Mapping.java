package edu.ohio.irg.bro.trace.map;

/**
 * Created with IntelliJ IDEA.
 * User: clarkg1
 * Date: 7/23/13
 * Time: 12:58 PM
 * To change this template use File | Settings | File Templates.
 */
public interface Mapping
{
    public String getName();
    public Integer getId();
}
