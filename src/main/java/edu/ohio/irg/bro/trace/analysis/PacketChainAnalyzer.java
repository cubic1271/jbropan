package edu.ohio.irg.bro.trace.analysis;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonWriter;
import edu.ohio.irg.bro.trace.TraceEntry;
import edu.ohio.irg.bro.trace.analysis.timing.ChainTimingAccumulator;
import edu.ohio.irg.bro.trace.exception.TraceRuntimeException;
import edu.ohio.irg.bro.trace.json.PacketChainAnalyzerAdapter;
import edu.ohio.irg.bro.trace.state.ChainContainer;
import edu.ohio.irg.bro.trace.state.GlobalEventIdentifier;
import edu.ohio.irg.bro.trace.util.EventNameResolver;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

public class PacketChainAnalyzer extends BasicAnalyzer {
    public static final int DEFAULT_MAX_CHAIN_SIZE = 2000000;
    public static final int DEFAULT_WEIGHT = 100;
    public static final String DEFAULT_NAME = PacketChainAnalyzer.class.getName();

    protected Stack<GlobalEventIdentifier> entries = new Stack<GlobalEventIdentifier>();
    protected ArrayList<TraceEntry> values = new ArrayList<TraceEntry>();
    protected HashMap<ChainContainer, Integer> chainCounts = new HashMap<ChainContainer, Integer>();
    protected HashMap<ChainContainer, ChainTimingAccumulator> timings = new HashMap<ChainContainer, ChainTimingAccumulator>();
    protected int maxChainSize = DEFAULT_MAX_CHAIN_SIZE;

    public PacketChainAnalyzer() {
        this.weight = DEFAULT_WEIGHT;
        this.name = DEFAULT_NAME;
    }

    public PacketChainAnalyzer(int weight) {
        this.weight = weight;
        this.name = DEFAULT_NAME;
    }

    public PacketChainAnalyzer(String name) {
        this.weight = DEFAULT_WEIGHT;
        this.name = name;
    }

    public PacketChainAnalyzer(int weight, String name) {
        this.weight = weight;
        this.name = name;
    }

    void postNewEntryEvent(TraceEntry entry) {

    }

    void preRemoveEntryEvent(TraceEntry entry) {

    }

    public HashMap<ChainContainer, ChainTimingAccumulator> getTimings() {
        return new HashMap<ChainContainer, ChainTimingAccumulator>(timings);
    }

    @Override
    public boolean update(TraceEntry entry) {
        super.update(entry);

        if(isSupported(entry)) {
            if(entry.isComponent() && entry.getGlobalId().getId().getFunctionId() == TraceEntry.ID_END_PACKET && !entries.empty()) {
                ChainContainer chain = new ChainContainer(entries);
                if(chainCounts.containsKey(chain)) {
                    chainCounts.put(chain, chainCounts.get(chain) + 1);
                }
                else {
                    chainCounts.put(chain, new Integer(1));
                }
                preRemoveEntryEvent(entry);
                entries.clear();
                if(!timings.containsKey(chain)) {
                    timings.put(chain, new ChainTimingAccumulator());
                }
                timings.get(chain).accumulate(values);
                values.clear();
            }

            if(entry.isCall() || entry.isComponent()) {
                GlobalEventIdentifier globalEventIdentifier = new GlobalEventIdentifier(entry);
                entries.push(globalEventIdentifier);
                values.add(entry);
                if(entries.size() > maxChainSize) {
                    throw new TraceRuntimeException("Maximum chain size exceeded (" + maxChainSize + ").");
                }
                postNewEntryEvent(entry);
            }
        }
        return true;
    }

    @Override
    public void clear() {
        super.clear();
        chainCounts.clear();
        entries.clear();
        values.clear();
        timings = new HashMap<ChainContainer, ChainTimingAccumulator>();
    }

    @Override
    public String toString() {
        return "PacketChainAnalyzer with " + chainCounts.size() + " unique chains after processing " + counter + " entries.";
    }

    public HashMap<ChainContainer, Integer> getChainCounts() {
        return new HashMap<ChainContainer, Integer>(chainCounts);
    }

    public int getMaxChainSize() {
        return maxChainSize;
    }

    public void setMaxChainSize(int maxChainSize) {
        this.maxChainSize = maxChainSize;
    }

    public HashMap<String, Integer> difference(PacketChainAnalyzer target, EventNameResolver resolver) {
        HashMap<String, Integer> difference = new HashMap<String, Integer>();
        // check to see what we have that the other analyzer doesn't ...
        for(ChainContainer container : chainCounts.keySet()) {
            if(target.chainCounts.containsKey(container.toString(resolver))) {
                difference.put(container.toString(resolver), chainCounts.get(container) - target.chainCounts.get(container));
            }
            else {
                difference.put(container.toString(resolver), chainCounts.get(container));
            }
        }

        // check to see what the other analyzer has that we don't ...
        // insert that into our difference map as negative values
        for(ChainContainer container : target.chainCounts.keySet()) {
            if(!chainCounts.containsKey(container)) {
                difference.put(container.toString(resolver), -target.chainCounts.get(container));
            }
        }

        return difference;
    }

    @Override
    public String toJSON() {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        try {
            writeJSON(output);
            return new String(output.toByteArray());
        }
        catch(IOException ex) {
            return null;
        }
    }

    @Override
    public void writeJSON(OutputStream output) throws IOException {
        JsonWriter writer = new JsonWriter(new OutputStreamWriter(output, "UTF-8"));
        writer.beginObject();
        writer.name("count").value(getCounter());
        writer.name("trial_id").value(getParent().getTrialId());
        writer.name("chains");
        writer.beginArray();
        HashMap<ChainContainer, Integer> counts = getChainCounts();
        HashMap<ChainContainer, ChainTimingAccumulator> timings = getTimings();

        for(ChainContainer container : counts.keySet()) {
            ArrayList<double[]> sums = timings.get(container).getSums();
            ArrayList<double[]> averages = timings.get(container).getAverages();
            ArrayList<double[]> variances = timings.get(container).getVariances();
            double[] chainSums = timings.get(container).getChainSums();
            double[] chainAverages = timings.get(container).getChainAverages();
            double[] chainVariances = timings.get(container).getChainVariances();
            int count = counts.get(container);

            writer.beginObject();
            writer.name("chain").value(container.toString());
            writer.name("count").value(count);
            writer.name("stats").beginObject();

            writer.name("sum").beginObject();
            for(int counter = 0; counter < ChainTimingAccumulator.TIME_NUM_COUNTERS; ++counter) {
                writer.name(ChainTimingAccumulator.TIME_FIELD_NAMES[counter]).value(chainSums[counter]);
            }
            writer.endObject(); // sum

            writer.name("average").beginObject();
            for(int counter = 0; counter < ChainTimingAccumulator.TIME_NUM_COUNTERS; ++counter) {
                writer.name(ChainTimingAccumulator.TIME_FIELD_NAMES[counter]).value(chainAverages[counter]);
            }
            writer.endObject(); // average

            writer.name("variance").beginObject();
            for(int counter = 0; counter < ChainTimingAccumulator.TIME_NUM_COUNTERS; ++counter) {
                writer.name(ChainTimingAccumulator.TIME_FIELD_NAMES[counter]).value(chainVariances[counter]);
            }
            writer.endObject(); // variance

            writer.endObject(); // stats
            writer.name("transitions").beginArray();
            for(int i = 0; i < sums.size(); ++i) {
                writer.beginObject();
                writer.name("from").value(container.get(i).toString());
                writer.name("to").value(container.get(i + 1).toString());
                writer.name("sum").beginObject();
                for(int counter = 0; counter < ChainTimingAccumulator.TIME_NUM_COUNTERS; ++counter) {
                    writer.name(ChainTimingAccumulator.TIME_FIELD_NAMES[counter]).value(sums.get(i)[counter]);
                }
                writer.endObject();  // sum
                writer.name("average").beginObject();
                for(int counter = 0; counter < ChainTimingAccumulator.TIME_NUM_COUNTERS; ++counter) {
                    writer.name(ChainTimingAccumulator.TIME_FIELD_NAMES[counter]).value(averages.get(i)[counter]);
                }
                writer.endObject();  // average
                writer.name("variance").beginObject();
                for(int counter = 0; counter < ChainTimingAccumulator.TIME_NUM_COUNTERS; ++counter) {
                    writer.name(ChainTimingAccumulator.TIME_FIELD_NAMES[counter]).value(variances.get(i)[counter]);
                }
                writer.endObject();  // variance
                writer.endObject();  // transitions array object
            }
            writer.endArray();
            writer.name("calls").beginArray();
            for(GlobalEventIdentifier id : container) {
                writer.value(id.toString());
            }
            writer.endArray();
            writer.endObject();
        }
        writer.endArray();
        writer.endObject();
        writer.close();
    }

}
