package edu.ohio.irg.bro.trace.exception;

import java.io.IOException;

public class InvalidTraceException extends IOException {
    public InvalidTraceException() {
        super();
    }

    public InvalidTraceException(String message) {
        super(message);
    }

    public InvalidTraceException(Exception ex) {
        super(ex);
    }

    public InvalidTraceException(String message, Exception ex) {
        super(message, ex);
    }
}
