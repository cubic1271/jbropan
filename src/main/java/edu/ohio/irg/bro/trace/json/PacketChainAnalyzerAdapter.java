package edu.ohio.irg.bro.trace.json;

import com.google.gson.*;
import edu.ohio.irg.bro.trace.analysis.PacketChainAnalyzer;
import edu.ohio.irg.bro.trace.analysis.timing.ChainTimingAccumulator;
import edu.ohio.irg.bro.trace.state.ChainContainer;
import edu.ohio.irg.bro.trace.state.GlobalEventIdentifier;
import edu.ohio.irg.bro.trace.util.EventNameResolver;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

public class PacketChainAnalyzerAdapter implements JsonSerializer<PacketChainAnalyzer> {
    protected transient EventNameResolver resolver;

    public PacketChainAnalyzerAdapter(EventNameResolver resolver) {
        this.resolver = resolver;
    }

    @Override
    public JsonElement serialize(PacketChainAnalyzer src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject obj = new JsonObject();
        obj.addProperty("count", src.getCounter());
        obj.addProperty("trial_id", src.getParent().getTrialId());

        JsonObject chains = new JsonObject();

        HashMap<ChainContainer, Integer> counts = src.getChainCounts();
        HashMap<ChainContainer, ChainTimingAccumulator> timings = src.getTimings();

        // int ctr = 0;
        for(ChainContainer container : counts.keySet()) {
            // ++ctr;
            // System.out.println("Writing chain (" + ctr + " / " + triggerTotals.keySet().size() + ")...");
            JsonObject curr = new JsonObject();
            int count = counts.get(container);
            curr.addProperty("count", count);

            JsonArray transitionArray = new JsonArray();
            ArrayList<double[]> sums = timings.get(container).getSums();
            ArrayList<double[]> averages = timings.get(container).getAverages();
            ArrayList<double[]> variances = timings.get(container).getVariances();
            for(int i = 0; i < sums.size(); ++i) {
                JsonObject timing = new JsonObject();
                JsonObject sum = new JsonObject();
                JsonObject average = new JsonObject();
                JsonObject variance = new JsonObject();
                for(int counter = 0; counter < ChainTimingAccumulator.TIME_NUM_COUNTERS; ++counter) {
                    sum.addProperty(ChainTimingAccumulator.TIME_FIELD_NAMES[counter], String.format("%.4g", sums.get(i)[counter]));
                    average.addProperty(ChainTimingAccumulator.TIME_FIELD_NAMES[counter], String.format("%.4g", averages.get(i)[counter]));
                    variance.addProperty(ChainTimingAccumulator.TIME_FIELD_NAMES[counter], String.format("%.4g", variances.get(i)[counter]));
                }
                timing.add("sum", sum);
                timing.add("average", average);
                timing.add("variance", variance);
                transitionArray.add(timing);
            }

            curr.add("transitions", transitionArray);

            JsonArray callArray = new JsonArray();
            for(GlobalEventIdentifier id : container) {
                callArray.add(new JsonPrimitive(id.toString()));
            }

            curr.add("calls", callArray);

            chains.add(container.toString(), curr);
        }

        obj.add("chains", chains);

        return obj;
    }
}
