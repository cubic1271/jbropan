package edu.ohio.irg.bro.trace.analysis.timing;

import edu.ohio.irg.bro.trace.TraceEntry;

public class EventTiming
{
    public static final int TIME_PAPI_VIRTUAL = 0;
    public static final int TIME_RUSAGE_SYSTEM = 1;
    public static final int TIME_RUSAGE_USER = 2;

    public static final String[] TIME_FIELD_NAMES = {"papiVirtual", "rusageSystem", "rusageUser"};

    public static final int TIME_NUM_COUNTERS = 3;

    long[] start;

    public EventTiming() {
        start = new long[TIME_NUM_COUNTERS];
        clear();
    }

    public EventTiming(TraceEntry entry) {
        start = new long[TIME_NUM_COUNTERS];
        start[TIME_PAPI_VIRTUAL] = entry.getPapiVirtual();
        start[TIME_RUSAGE_SYSTEM] = entry.getSystemTime();
        start[TIME_RUSAGE_USER] = entry.getUserTime();
    }

    public void clear() {
        for(int i = 0; i < TIME_NUM_COUNTERS; ++i) {
            start[i] = 0;
        }
    }

    public void start(int index, long value) {
        start[index] = value;
    }

    public long[] elapsed(EventTiming curr) {
        long[] differences = new long[TIME_NUM_COUNTERS];
        long[] updated = curr.array();
        for(int i = 0; i < TIME_NUM_COUNTERS; ++i) {
            differences[i] = updated[i] - start[i];
        }
        return differences;
    }

    public long[] array() {
        return start;
    }

    @Override
    public String toString() {
        StringBuilder rval = new StringBuilder();
        for(int i = 0; i < TIME_NUM_COUNTERS; ++i) {
            rval.append(TIME_FIELD_NAMES[i]).append(": ").append(start[i]);
        }
        return rval.toString();
    }
}
