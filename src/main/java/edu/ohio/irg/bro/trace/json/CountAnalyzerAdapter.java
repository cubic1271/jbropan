package edu.ohio.irg.bro.trace.json;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import edu.ohio.irg.bro.trace.analysis.CountAnalyzer;
import edu.ohio.irg.bro.trace.state.GlobalEventIdentifier;
import edu.ohio.irg.bro.trace.util.EventNameResolver;

import java.lang.reflect.Type;
import java.util.HashMap;

public class CountAnalyzerAdapter implements JsonSerializer<CountAnalyzer> {
    protected transient EventNameResolver resolver;

    public CountAnalyzerAdapter(EventNameResolver resolver) {
        this.resolver = resolver;
    }

    @Override
    public JsonElement serialize(CountAnalyzer src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject obj = new JsonObject();
        obj.addProperty("trial_id", src.getParent().getTrialId());

        JsonObject scriptCountObject = new JsonObject();
        JsonObject componentCountObject = new JsonObject();
        obj.addProperty("count", src.getCounter());

        HashMap<GlobalEventIdentifier, Integer> counts = src.getCounts();

        for(GlobalEventIdentifier curr : counts.keySet()) {
            if(curr.getType() == GlobalEventIdentifier.EntryType.SCRIPT) {
                if(scriptCountObject.has(curr.toString())) {
                    // System.err.println("Warning: multiple global IDs resolve to a single shared name...");
                    scriptCountObject.remove(curr.toString());
                }
                scriptCountObject.addProperty(curr.toString(), counts.get(curr));
            }
            else if(curr.getType() == GlobalEventIdentifier.EntryType.COMPONENT) {
                if(componentCountObject.has(curr.toString())) {
                    // System.err.println("Warning: multiple global IDs resolve to a single shared name...");
                    componentCountObject.remove(curr.toString());
                }
                componentCountObject.addProperty(curr.toString(), counts.get(curr));
            }
        }
        obj.add("scriptCounts", scriptCountObject);
        obj.add("componentCounts", componentCountObject);
        return obj;
    }
}
