package edu.ohio.irg.bro.trace.state;

import org.junit.Test;

import static org.junit.Assert.*;

public class ChainContainerTest {
    @Test
    public void testEquals() throws Exception {
        GlobalEventIdentifier e1 = new GlobalEventIdentifier(new LocalEventIdentifier(22, 49), GlobalEventIdentifier.EntryType.COMPONENT);
        GlobalEventIdentifier e2 = new GlobalEventIdentifier(new LocalEventIdentifier(22, 49), GlobalEventIdentifier.EntryType.SCRIPT);
        GlobalEventIdentifier e3 = new GlobalEventIdentifier(new LocalEventIdentifier(22, 50), GlobalEventIdentifier.EntryType.COMPONENT);
        GlobalEventIdentifier e4 = new GlobalEventIdentifier(new LocalEventIdentifier(22, 50), GlobalEventIdentifier.EntryType.SCRIPT);
        GlobalEventIdentifier e5 = new GlobalEventIdentifier(new LocalEventIdentifier(22, 49), GlobalEventIdentifier.EntryType.COMPONENT);
        GlobalEventIdentifier e6 = new GlobalEventIdentifier(new LocalEventIdentifier(22, 49), GlobalEventIdentifier.EntryType.SCRIPT);

        ChainContainer c1 = new ChainContainer();
        ChainContainer c2 = new ChainContainer();

        assertEquals(c1, c2);

        c1.add(e1);
        c1.add(e2);
        c1.add(e3);
        c1.add(e4);
        c1.add(e5);
        c1.add(e4);

        c2.add(e1);
        c2.add(e2);
        c2.add(e3);
        c2.add(e4);
        c2.add(e5);
        c2.add(e4);

        assertEquals(c1, c2);

        c2.clear();

        c2.add(e2);
        c2.add(e1);
        c2.add(e3);
        c2.add(e4);
        c2.add(e5);
        c2.add(e4);

        assertNotEquals(c1, c2);

        c2.clear();
        c2.add(e1);

        c1.clear();
        c1.add(e1);

        assertEquals(c1, c2);
    }

    @Test
    public void testHashCode() throws Exception {
        GlobalEventIdentifier e1 = new GlobalEventIdentifier(new LocalEventIdentifier(22, 49), GlobalEventIdentifier.EntryType.COMPONENT);
        GlobalEventIdentifier e2 = new GlobalEventIdentifier(new LocalEventIdentifier(22, 49), GlobalEventIdentifier.EntryType.SCRIPT);
        GlobalEventIdentifier e3 = new GlobalEventIdentifier(new LocalEventIdentifier(22, 50), GlobalEventIdentifier.EntryType.COMPONENT);
        GlobalEventIdentifier e4 = new GlobalEventIdentifier(new LocalEventIdentifier(22, 50), GlobalEventIdentifier.EntryType.SCRIPT);
        GlobalEventIdentifier e5 = new GlobalEventIdentifier(new LocalEventIdentifier(22, 49), GlobalEventIdentifier.EntryType.COMPONENT);
        GlobalEventIdentifier e6 = new GlobalEventIdentifier(new LocalEventIdentifier(22, 49), GlobalEventIdentifier.EntryType.SCRIPT);

        ChainContainer c1 = new ChainContainer();
        ChainContainer c2 = new ChainContainer();

        assertEquals(c1.hashCode(), c2.hashCode());

        c1.add(e1);
        c1.add(e2);
        c1.add(e3);
        c1.add(e4);
        c1.add(e5);
        c1.add(e4);

        c2.add(e1);
        c2.add(e2);
        c2.add(e3);
        c2.add(e4);
        c2.add(e5);
        c2.add(e4);

        assertEquals(c1.hashCode(), c2.hashCode());

        c2.clear();

        c2.add(e2);
        c2.add(e1);
        c2.add(e3);
        c2.add(e4);
        c2.add(e5);
        c2.add(e4);

        assertNotEquals(c1.hashCode(), c2.hashCode());

        c2.clear();
        c2.add(e1);

        c1.clear();
        c1.add(e1);

        assertEquals(c1.hashCode(), c2.hashCode());
    }
}
