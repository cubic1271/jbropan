package edu.ohio.irg.bro.trace.state;

import org.junit.Test;

import static org.junit.Assert.*;

public class LocalEventIdentifierTest {
    @Test
    public void testEquals() throws Exception {
        LocalEventIdentifier e1 = new LocalEventIdentifier(22, 49);
        LocalEventIdentifier e2 = new LocalEventIdentifier(22, 49);
        LocalEventIdentifier e3 = new LocalEventIdentifier(22, 50);
        LocalEventIdentifier e4 = new LocalEventIdentifier(22, 50);
        LocalEventIdentifier e5 = new LocalEventIdentifier(23, 50);
        LocalEventIdentifier e6 = new LocalEventIdentifier(23, 50);

        assertEquals(e1, e2);
        assertEquals(e3, e4);
        assertEquals(e5, e6);
        assertNotEquals(e1, e3);
        assertNotEquals(e1, e5);
        assertNotEquals(e3, e5);
    }

    @Test
    public void testHashCode() throws Exception {
        LocalEventIdentifier e1 = new LocalEventIdentifier(22, 49);
        LocalEventIdentifier e2 = new LocalEventIdentifier(22, 49);
        LocalEventIdentifier e3 = new LocalEventIdentifier(22, 50);
        LocalEventIdentifier e4 = new LocalEventIdentifier(22, 50);
        LocalEventIdentifier e5 = new LocalEventIdentifier(23, 50);
        LocalEventIdentifier e6 = new LocalEventIdentifier(23, 50);

        assertEquals(e1.hashCode(), e2.hashCode());
        assertEquals(e3.hashCode(), e4.hashCode());
        assertEquals(e5.hashCode(), e6.hashCode());
        assertNotEquals(e1.hashCode(), e3.hashCode());
        assertNotEquals(e1.hashCode(), e5.hashCode());
        assertNotEquals(e3.hashCode(), e5.hashCode());
    }
}
