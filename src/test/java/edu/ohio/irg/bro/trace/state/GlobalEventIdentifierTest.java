package edu.ohio.irg.bro.trace.state;

import org.junit.Test;
import static org.junit.Assert.*;

public class GlobalEventIdentifierTest {
    @Test
    public void testEquals() throws Exception {
        GlobalEventIdentifier e1 = new GlobalEventIdentifier(new LocalEventIdentifier(22, 49), GlobalEventIdentifier.EntryType.COMPONENT);
        GlobalEventIdentifier e2 = new GlobalEventIdentifier(new LocalEventIdentifier(22, 49), GlobalEventIdentifier.EntryType.SCRIPT);
        GlobalEventIdentifier e3 = new GlobalEventIdentifier(new LocalEventIdentifier(22, 50), GlobalEventIdentifier.EntryType.COMPONENT);
        GlobalEventIdentifier e4 = new GlobalEventIdentifier(new LocalEventIdentifier(22, 50), GlobalEventIdentifier.EntryType.SCRIPT);
        GlobalEventIdentifier e5 = new GlobalEventIdentifier(new LocalEventIdentifier(22, 49), GlobalEventIdentifier.EntryType.COMPONENT);
        GlobalEventIdentifier e6 = new GlobalEventIdentifier(new LocalEventIdentifier(22, 49), GlobalEventIdentifier.EntryType.SCRIPT);

        assertEquals(e1, e5);
        assertEquals(e2, e6);
        assertNotEquals(e3, e5);
        assertEquals(e4, e4);
        assertNotEquals(e5, e6);
        assertNotEquals(e6, e1);
    }

    @Test
    public void testHashCode() throws Exception {
        GlobalEventIdentifier e1 = new GlobalEventIdentifier(new LocalEventIdentifier(22, 49), GlobalEventIdentifier.EntryType.COMPONENT);
        GlobalEventIdentifier e2 = new GlobalEventIdentifier(new LocalEventIdentifier(22, 49), GlobalEventIdentifier.EntryType.SCRIPT);
        GlobalEventIdentifier e3 = new GlobalEventIdentifier(new LocalEventIdentifier(22, 50), GlobalEventIdentifier.EntryType.COMPONENT);
        GlobalEventIdentifier e4 = new GlobalEventIdentifier(new LocalEventIdentifier(22, 50), GlobalEventIdentifier.EntryType.SCRIPT);
        GlobalEventIdentifier e5 = new GlobalEventIdentifier(new LocalEventIdentifier(22, 49), GlobalEventIdentifier.EntryType.COMPONENT);
        GlobalEventIdentifier e6 = new GlobalEventIdentifier(new LocalEventIdentifier(22, 49), GlobalEventIdentifier.EntryType.SCRIPT);

        assertEquals(e1.hashCode(), e5.hashCode());
        assertEquals(e2.hashCode(), e6.hashCode());
        assertNotEquals(e3.hashCode(), e5.hashCode());
        assertEquals(e4.hashCode(), e4.hashCode());
        assertNotEquals(e5.hashCode(), e6.hashCode());
        assertNotEquals(e6.hashCode(), e1.hashCode());
    }
}

